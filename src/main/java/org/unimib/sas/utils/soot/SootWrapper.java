package org.unimib.sas.utils.soot;
import soot.options.*;
import soot.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SootWrapper {
    public static SootWrapper v = new SootWrapper();

    private List<String> arguments;
    private StringBuilder scpBuilder;
    public SootWrapper(){
        arguments = new ArrayList<>();
        scpBuilder = new StringBuilder();
        //withClasspath(Paths.get("lib","soot-trunk.jar").toString());
        withClasspath(Paths.get(System.getProperty("java.home"), "lib", "jce.jar").toString());
        withClasspath(Paths.get(System.getProperty("java.home"), "lib", "rt.jar").toString());
        //withClasspath(Paths.get(javaHomeDir, "lib", "jce.jar").toString());

        //withOutputDir("sootOutput");
    }

    /**
     * Adds a string to Soot's classpath
     * @param classPaths the path to add to the classpath
     * @return The this SootWrapper instance.
     */
    public SootWrapper withClasspath(String... classPaths){
        String sootClasspath = Options.v().soot_classpath();
        StringBuilder scpBuilder = new StringBuilder();
        scpBuilder.append(sootClasspath);
        if (!sootClasspath.isEmpty() && sootClasspath.charAt(sootClasspath.length() - 1) != File.pathSeparatorChar) {
            scpBuilder.append(File.pathSeparator);
        }
        for (int i = 0; i < classPaths.length; i++){
            scpBuilder.append(classPaths[i]);
            if (i < classPaths.length - 1)
                scpBuilder.append(File.pathSeparator);
        }
        Options.v().set_soot_classpath(scpBuilder.toString());
        return this;
    }

    /**
     * Set the phantom refs option.
     * @param setting the value to set
     * @return The this SootWrapper instance.
     */
    public SootWrapper withPhantomRefs(boolean setting){
        Options.v().set_allow_phantom_refs(setting);
        return v;
    }

    /**
     * Sets the main class for soot. If not set, soot will automatically infer a main class.
     * @param setting The name of the main class.
     * @return The this SootWrapper instance.
     */
    public SootWrapper withMainClass(String setting){
        Options.v().set_main_class(setting);
        return v;
    }

    /**
     * Sets the output dir of soot.
     * @param setting The output dir path.
     * @return The this SootWrapper instance.
     */
    public SootWrapper withOutputDir(String setting){
        Options.v().set_output_dir(setting);
        return v;
    }

    /**
     * Adds a phase to use to soot execution.
     * @param phaseName The phase name to add.
     * @return The this SootWrapper instance.
     */
    public SootWrapper withPhase(String phaseName){
        return withPhase(phaseName, "enabled:true");
    }

    /**
     * Adds a phase to use to soot execution.
     * @param phaseName The phase name to add.
     * @param phaseOptions The phase commands in opt:value format.
     * @return The this SootWrapper instance.
     */
    public SootWrapper withPhase(String phaseName, String phaseOptions){
        arguments.add("-p");
        arguments.add(phaseName);
        arguments.add(phaseOptions);
        return v;
    }

    /**
     * Adds a transformation to the given phase.
     * @param phase the phase to modify.
     * @param myTransform the transformation object containing the code to execute during the phase.
     * @return The this SootWrapper instance.
     */
    public SootWrapper withPack(String phase, Transform myTransform){
        PackManager.v().getPack(phase).add(myTransform);
        return v;
    }

    /**
     * Adds the given arguments to the args buffer.
     * @param args the arguments to add to soot arguments list
     * @return The this SootWrapper instance.
     */
    public SootWrapper withArgs(String... args){
        for (int i = 0; i < args.length; i++){
            arguments.add(args[i]);
        }
        return v;
    }

    /**
     * Sets the whole program option.
     * @param setting The value of the whole program option.
     * @return The this SootWrapper instance.
     */
    public SootWrapper withWholeProgram(boolean setting){
        Options.v().set_whole_program(setting);
        return v;
    }

    /**
     * Adds the given directories, or jar files, to the files to be processed.
     * @param dirs The directories or the jar files to process.
     * @return The this SootWrapper instance.
     */
    public SootWrapper withProcessDir(String... dirs){
        List<String> list = new ArrayList<>();
        for (String s : dirs){
            list.add(s);
        }
        withProcessDir(list);
        return v;
    }

    /**
     * Adds the given directories, or jar files, to the files to be processed.
     * @param dirs The directories or the jar files to process.
     * @return The this SootWrapper instance.
     */
    public SootWrapper withProcessDir(List<String> dirs) {
        dirs.addAll(Options.v().process_dir());
        Options.v().set_process_dir(dirs);
        return v;
    }

    /**
     * Runs soot directly with the given args, others.
     * @param args The args to pass to soot.
     */
    public void runWithArgs(String args[]){
        soot.Main.v().run(args);
    }

    /**
     * Runs soot on the given target with the arguments set.
     * @param target The target class file(s). Use space to give more classes.
     */
    public void run(String target){
        arguments.add(target);
        runWithArgs(arguments.toArray(new String[0]));
        //runWithArgs(new String[]{"-p", "cg", "enabled:true,all-reachable:true,implicit-entry:false",
        //        "-p", "jb", -p", "wjtp", "enabled:true",  target});
    }

    /**
     * Adds all the given args to the current arguments and then executes Soot.
     * @param args An additional list of arguments to pass to soot.
     */
    public void run(String... args){
        arguments.addAll(Arrays.asList(args));
        runWithArgs(arguments.toArray(new String[0]));
    }

    /**
     * Runs soot without a given target class to transform.
     */
    public void run(){
        runWithArgs(arguments.toArray(new String[0]));
    }
}
