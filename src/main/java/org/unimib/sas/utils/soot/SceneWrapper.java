package org.unimib.sas.utils.soot;

import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.jimple.infoflow.data.SootMethodAndClass;
import soot.jimple.infoflow.util.SootMethodRepresentationParser;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.util.Chain;
import soot.util.HashChain;

public class SceneWrapper {

    public static SceneWrapper v = new SceneWrapper();

    /**
     * Get all the classes of the current soot Scene.
     * @return All the classes from the current Scene.
     */
    public Chain<SootClass> getClasses(){
        return Scene.v().getClasses();
    }

    /**
     * Returns the soot class with the given name.
     * @param name The name of the class to search.
     * @return A soot class with the given name.
     */
    public SootClass getClass(String name){
        Chain<SootClass> classes = getClasses();
        for (SootClass sc : classes){
            if (sc.getName().equals(name))
                return sc;
        }
        return null;
    }

    /**
     * Filters all the interfaces in the current soot Scene.
     * @return A chain containing only interfaces
     */
    public Chain<SootClass> getInterfaces(){
        Chain<SootClass> classes = getClasses();
        Chain<SootClass> interfaces = new HashChain<>();
        for (SootClass sc: classes) {
            if(sc.isInterface()){
                interfaces.addLast(sc);
            }
        }
        return interfaces;
    }

    /**
     * Calculates if the given class implements the given interface.
     * @param className The target class.
     * @param interfaceName The interface name.
     * @return True if the given class implements the given interface, false otherwise.
     */
    public boolean implementsInterface(String className, String interfaceName){
        SootClass sc = getClass(className);
        return sc.implementsInterface(interfaceName);
    }

    /**
     * Calculates if the given class has a parent class with the given name.
     * @param className The child class name.
     * @param parentName The super class name.
     * @return True if the given class extends the given parent class.
     */
    public boolean hasSuperclass(String className, String parentName){
        return hasSuperclass(getClass(className), parentName);
    }

    /**
     * Calculates if the given class has a parent class with the given name.
     * @param sc The child class.
     * @param parentName The super class name.
     * @return True if the given class extends the given parent class.
     */
    public boolean hasSuperclass(SootClass sc, String parentName){
        if(sc.getName().equals("java.lang.Object") || !sc.hasSuperclass())
            return false;
        else{
            SootClass sp = sc.getSuperclass();
            if (!sp.getName().equals(parentName))
                return hasSuperclass(sp, parentName);
            else
                return true;
        }
    }
    /**
     * Calcutes if the given class has a parent class.
     * @param className The base class name.
     * @return True if the given class has a super class.
     */
    public boolean hasSuperclass(String className){
        if (className.equals("java.lang.Object"))
            return false;
        return getClass(className).hasSuperclass();
    }

    /**
     * Forces soot to resolve the class name, if it's not resolved and the class is phantom null is returned.
     * @param className The name of the class to retrive.
     * @return The soot class if it is resolved and is NOT a phantom class, null otherwise.
     */
    public SootClass getSootClass(String className){
        SootClass sc = Scene.v().getSootClass(className);
        if (sc == null){
            sc = Scene.v().forceResolve(className, SootClass.BODIES);
        }
        if (sc.isPhantom())
            sc = null;
        return sc;
    }

    /**
     * The methods forces soot to resolve the class for the given signature, if it fails it returns null.
     * @param signature The signature of the method to grab.
     * @return The method or null if it is not found.
     */
    public SootMethod grabMethod(String signature){
        SootMethod sm;
        SootMethodAndClass sam = SootMethodRepresentationParser.v().parseSootMethodString(signature);
        SootClass sc = Scene.v().forceResolve(sam.getClassName(), SootClass.BODIES);
        if (sc == null || sc.isPhantom())
            sm = null;
        else
            sm = sc.getMethod(sam.getSubSignature());
        return sm;
    }

    /**
     * Returns the call graph of the program under analysis.
     * To activate the call graph you must activate "cg" phase.
     * @return The call graph of the program.
     */
    public CallGraph getCallGraph(){
        return Scene.v().getCallGraph();
    }
}
