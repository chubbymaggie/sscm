package org.unimib.sas.tdfa;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import soot.*;
import soot.jimple.*;
import soot.toolkits.graph.ExceptionalUnitGraph;

import java.util.*;

/**
 * Calculates taint analysis on a given method body with the given starting tainted locals.
 * The formulae used to apply taint analysis are:
 *      let M = pred(n) the set of all predecessors of node n
 *
 *      TaintIn(n) = Union for each m in M of TaintOut(m) minus sanitized variables of the current method
 *      TaintUse(n) = use(n) intersected with TaintIn(n)
 *      TaintOut(n) = TaintIn(n)                    iff TaintUse == {}
 *                  = TaintIn(n) union def(n)       else
 */
public class TaintAnalysis {

    /**
     * A simple default list of prefixes for identify source methods.
     */
    private final static String[] defaultSourceMethodPrefixes = {
            "get", "find", "retrieve", "as", "read", "pull",
            "remove", "peek", "poll", "element", "preleva", "prendi",
    };

    /**
     * A simple default list of prefixes for identify sink methods.
     */
    private final static String[] defaultSinkMethodPrefix = {
            "set", "write", "add", "put", "insert", "push", "offer"
    };

    private List<String> sourceMethodPrefixes;
    private List<String> sinkMethodPrefixes;

    /**
     * Creates a new taint analysis object using the default source and sinks prefixes.
     */
    public TaintAnalysis(){
        this(defaultSourceMethodPrefixes, defaultSinkMethodPrefix);
    }

    /**
     * Creates a new taint analysis object using the given prefixes for source and sink methods.
     * @param sourcePrefixes the prefixes to use to identify source methods.
     * @param sinkPrefixes the prefixes to use to identify sink methods.
     */
    public TaintAnalysis(String[] sourcePrefixes, String[]  sinkPrefixes){
        this.sourceMethodPrefixes = new ArrayList<>();
        Collections.addAll(this.sourceMethodPrefixes, sourcePrefixes);
        this.sinkMethodPrefixes = new ArrayList<>();
        Collections.addAll(this.sinkMethodPrefixes, sinkPrefixes);
    }

    /**
     * Creates a new taint analysis object using the given prefixes for source and sink methods.
     * @param sourcePrefixes the prefixes to use to identify source methods.
     * @param sinkPrefixes the prefixes to use to identify sink methods.
     */
    public TaintAnalysis(@NotNull List<String> sourcePrefixes, @NotNull List<String> sinkPrefixes){
        this.sourceMethodPrefixes = new ArrayList<>(sourcePrefixes);
        this.sinkMethodPrefixes = new ArrayList<>(sinkPrefixes);
    }

    /**
     * Executes taint analysis on the given method body.
     * @param body The body of the method.
     * @param startingTaintedLocals the starting tainted locals
     * @return A set containing all the taint-out variables for each statement in the body.
     */
    @NotNull
    public HashMap<Unit, HashSet<Value>> taintAnalysisLocals(@NotNull Body body, HashSet<Value> startingTaintedLocals){
        return taintAnalysis(body, startingTaintedLocals, new HashSet<>());
    }

    /**
     * Executes taint analysis on the given method body.
     * @param body The body of the method.
     * @param startingTaintedMethodCalls the starting tainted method invocation
     * @return A set containing all the taint-out variables for each statement in the body.
     */
    @NotNull
    public HashMap<Unit, HashSet<Value>> taintAnalysisMethodCalls(@NotNull Body body, @NotNull HashSet<InvokeExpr> startingTaintedMethodCalls){
        return taintAnalysis(body, new HashSet<>(), startingTaintedMethodCalls);
    }

    /**
     * Executes taint analysis on the given method body.
     * @param body The body of the method.
     * @param startingTaintedLocals the starting tainted locals
     * @param startingTaintedMethodCalls the starting method calls to consider as sources of tainted Values
     * @return A set containing all the taint-out variables for each statement in the body.
     */
    @NotNull
    public HashMap<Unit, HashSet<Value>> taintAnalysis(@NotNull Body body, HashSet<Value> startingTaintedLocals, @NotNull HashSet<InvokeExpr> startingTaintedMethodCalls){
        // Inizializzazione
        ExceptionalUnitGraph cfg = new ExceptionalUnitGraph(body);
        HashMap<Unit, HashSet<Value>> taintOut = new HashMap<>();
        HashMap<Unit, HashSet<Value>> taintIn = new HashMap<>();
        HashSet<Value> sanitized = new HashSet<>();

        // Imposta i Value tainted iniziali a tutti gli entry point
        for (Unit head : cfg.getHeads()){
            taintIn.put(head, startingTaintedLocals);
            taintOut.put(head, startingTaintedLocals);
        }

        PatchingChain<Unit> units = body.getUnits();
        Queue<Unit> workList = new LinkedList<>(units);

        while(!workList.isEmpty()){
            Unit statement = workList.remove();
            HashSet<Value> oldVal = new HashSet<>(taintOut.getOrDefault(statement, new HashSet<>()));

            // safe check to ensure every statement has a HashSet
            if (!taintIn.containsKey(statement))
                taintIn.put(statement, new HashSet<>());

            // Per ogni predecessore di statement, fa l'unione di tutti i taint out e salvalo nel taintIn di statement
            cfg.getUnexceptionalPredsOf(statement).forEach(u -> {
                HashSet<Value> inSet = taintIn.getOrDefault(statement, new HashSet<>());
                inSet.addAll(taintOut.getOrDefault(u, new HashSet<>()));
                taintIn.put(statement, inSet);
            });

            // Remove sanitezed variables
            taintIn.get(statement).removeAll(sanitized);
            sanitized.clear();

            // Calcola il nuovo taintOut per statement seguendo le regole di taint definite
            HashSet<Value> outSet = taintOut.getOrDefault(statement, new HashSet<>());
            outSet.addAll(taintIn.getOrDefault(statement, new HashSet<>()));
            if (!taintUse(taintIn, statement).isEmpty()) {
                // Se viene assegnato a un field di un oggetto un valore taint => anche l'oggetto è tainted (anche array)
                if (statement instanceof AssignStmt && !((AssignStmt) statement).containsInvokeExpr()){
                    AssignStmt assi = (AssignStmt)statement;
                    if (assi.getLeftOp() instanceof ArrayRef) {
                        outSet.add(((ArrayRef) assi.getLeftOp()).getBase());
                        Value originalArrRef = getOriginalArrayRef(statement, body);
                        if (originalArrRef != null)
                            outSet.add(originalArrRef);
                    } else {
                        outSet.add(getTaintedLeftOp((AssignStmt) statement));
                    }
                // Se RHS è un FieldRef di un oggetto tainted =>  LHS è tainted
                }else if(statement instanceof AssignStmt && ((AssignStmt) statement).containsFieldRef() &&
                        ((AssignStmt) statement).getFieldRefBox().equals(((AssignStmt) statement).getRightOpBox())){
                    FieldRef fieldRef = ((AssignStmt) statement).getFieldRef();
                    if (fieldRef instanceof InstanceFieldRef){
                        if (outSet.contains(((InstanceFieldRef) fieldRef).getBase()))
                        outSet.add(((AssignStmt) statement).getLeftOp());
                    }

                // Se almeno un parametro del metodo invocato in RHS è taint => anche LHS lo è
                }else if (statement instanceof AssignStmt && ((AssignStmt) statement).containsInvokeExpr()){
                    InvokeExpr invkExpr = ((AssignStmt) statement).getInvokeExpr();

                    // Se il metodo è chiamato su un oggetto tainted => LHS è tainted
                    if (invkExpr instanceof InstanceInvokeExpr && outSet.contains(((InstanceInvokeExpr) invkExpr).getBase())){
                        outSet.add(((AssignStmt) statement).getLeftOp());

                    // Altrimenti controlla se il metodo utilizza parametri tainted e tainta LHS
                    }else{
                        List<Value> methodArgs = invkExpr.getArgs();
                        for (Value arg : methodArgs) {
                            if (outSet.contains(arg)) {
                                outSet.add(((AssignStmt) statement).getLeftOp());
                                break;
                            }
                        }
                    }

                // Se viene chiamato un metodo di un oggetto con parametri tainted => segna l'oggetto come tainted
                }else if (statement instanceof InvokeStmt && isInstanceInvokeStmt(statement)){
                    InstanceInvokeExpr invkExpr = (InstanceInvokeExpr)((InvokeStmt) statement).getInvokeExpr();
                    List<Value> methodArgs = invkExpr.getArgs();
                    for (Value arg : methodArgs){
                        if (outSet.contains(arg)){
                            Value originalInvokingObject = getOriginalInvokingObject(statement, body);
                            if(originalInvokingObject != null){
                                outSet.add(originalInvokingObject);
                            }
                            outSet.add(invkExpr.getBase());
                            break;
                        }
                    }
                }

            // Control if there is a method considered as source called and
            // threat its return value (or object used as argument) as tainted
            }else if (statement instanceof AssignStmt && ((AssignStmt) statement).containsInvokeExpr()){
                InvokeExpr invkExpr = ((AssignStmt) statement).getInvokeExpr();
                if(startingTaintedMethodCalls.contains(invkExpr)){
                    outSet.add(((AssignStmt) statement).getLeftOp());
                    invkExpr.getArgs().forEach(value -> {
                        if (value.getType() instanceof RefLikeType){
                            outSet.add(getOriginalValue(statement, body, value));
                        }
                    });
                }else {
                    // No tainted variable was used and no source method was used -> sanitize LOP
                    sanitized.add(((AssignStmt) statement).getLeftOp());
                }
            } else if(statement instanceof InvokeStmt) {
                InvokeExpr invkExpr = ((InvokeStmt) statement).getInvokeExpr();
                if (startingTaintedMethodCalls.contains(invkExpr)) {
                    invkExpr.getArgs().forEach(value -> {
                        if (value.getType() instanceof RefLikeType) {
                            outSet.add(getOriginalValue(statement, body, value));
                        }
                    });
                }
            }else if(statement instanceof AssignStmt){
                // Nessuna variabile taint è stata usata e nessun metodo sorgente è stato usato -> sanitizza lop
                sanitized.add(((AssignStmt) statement).getLeftOp());
            }
            taintOut.put(statement, outSet);

            // Controlla se è cambiato, se non è cambiato allora non rivisitare questo nodo/unit
            if(!taintOut.get(statement).equals(oldVal)){
                List<Unit> successors = cfg.getUnexceptionalSuccsOf(statement);
                workList.addAll(successors);
            }
        }
        lastTaintIn = taintIn;
        return taintOut;
    }

    /**
     * Controls if the given Unit is an instance InvokeStmt and its invoke is a instance of InstanceInvokeExpr
     * @param statement the statement to control
     * @return True if statement is a InvokeStmt and its expression is a InstanceInvokeExpr, false otherwise.
     */
    private boolean isInstanceInvokeStmt(Unit statement) {
        return statement instanceof InvokeStmt && ((InvokeStmt)statement).getInvokeExpr() instanceof InstanceInvokeExpr;
    }

    /**
     * Returns the correct value of the taint of an assign statement:
     *  if the left operand is a instance field then the field's object is returned
     *  otherwise the left op Value is returned.
     *  It is assumed that the RHS is Tainted!
     * @param stmt the statement under analysis.
     * @return A Value containing the local variable tainted.
     */
    private Value getTaintedLeftOp(@NotNull AssignStmt stmt){
        ValueBox lhs = stmt.getLeftOpBox();
        if (stmt.containsFieldRef() && stmt.getFieldRefBox().equals(lhs) && lhs.getValue() instanceof InstanceFieldRef){
            InstanceFieldRef instFieldRef = (InstanceFieldRef)lhs.getValue();
            return instFieldRef.getBase();
        }else if(stmt.containsArrayRef() && stmt.getArrayRef().equals(lhs)) {
            ArrayRef arrRef = (ArrayRef) lhs;
            return arrRef.getBase();
        }else{
            return stmt.getLeftOp();
        }
    }

    /**
     * Searches for the original local Value used for the array.
     * Returns null if the current statement is not an assign statement containing an array ref
     * @param statement the current statement.
     * @param body the body of the method
     * @return the original local variable of the array
     */
    @Nullable
    private Value getOriginalArrayRef(Unit statement, @NotNull Body body){
        if(!(statement instanceof AssignStmt && ((AssignStmt) statement).containsArrayRef()))
            return null;
        Value appoggio = ((AssignStmt) statement).getArrayRef().getBase();
        return getOriginalValue(statement, body, appoggio);
    }

    /**
     * Returns the original object on which was called the invoke.
     * In Jimple if you call a method on a 'this' class attribute a temporal variable is used to store
     * the reference:
     *   $r3 = r0.<tests.GuineaPigTestClass: java.tdfa.List strs>;
     *   interfaceinvoke $r3.<java.tdfa.List: boolean add(java.lang.Object)>($r2);
     * @param statement the statement containing the 'fake control object'
     * @param body The body of the statement.
     * @return The Value object on which invkExpr is invoked
     */
    private Value getOriginalInvokingObject(Unit statement, @NotNull Body body){
        InvokeExpr invkExpr = null;
        if (statement instanceof AssignStmt && ((AssignStmt) statement).containsInvokeExpr()){
            invkExpr = ((AssignStmt) statement).getInvokeExpr();
        }else if(statement instanceof InvokeStmt){
            invkExpr = ((InvokeStmt) statement).getInvokeExpr();
        }
        if (invkExpr == null || !(invkExpr instanceof InstanceInvokeExpr))
            return null;

        InstanceInvokeExpr instInvkExpr = (InstanceInvokeExpr)invkExpr;
        Value tmpRef = instInvkExpr.getBase();
        return getOriginalValue(statement, body, tmpRef);
    }

    /**
     * Search the original reference local value.
     * @param statement The current statement
     * @param body the body of the method
     * @param appoggio the Value used to mask the original Value
     * @return Returns the original Value of the given 'appoggio' Value or 'appoggio' is nothing is found.
     */
    private Value getOriginalValue(Unit statement, @NotNull Body body, Value appoggio) {
        PatchingChain<Unit> units = body.getUnits();
        Unit predUn = statement;
        Value found = appoggio;
        while((predUn = units.getPredOf(predUn)) != null) {
            if (predUn instanceof AssignStmt) {
                if (((AssignStmt) predUn).getLeftOp().equals(appoggio) && ((AssignStmt) predUn).getRightOp() instanceof InstanceFieldRef){
                    InstanceFieldRef field = (InstanceFieldRef)((AssignStmt) predUn).getFieldRef();
                    found = field.getBase();
                    break;
                }
            }
        }
        return found;
    }

    /**
     * Calculates the tainted variables used in the given unit (statement).
     * @param taintIn The taint-in variables set.
     * @param unit The current statement/unit.
     * @return The hashset of the used tainted variables in the current statement.
     */
    @NotNull
    private  HashSet<Value> taintUse(@NotNull HashMap<Unit, HashSet<Value>> taintIn, @NotNull Unit unit) {
        HashSet<Value> result = new HashSet<>();
        unit.getUseBoxes().forEach(valueBox -> result.add(valueBox.getValue()));
        result.retainAll(taintIn.getOrDefault(unit, new HashSet<>()));
        return result;
    }

    /**
     * Gets the source method prefixes list.
     * @return a prefix list of strings.
     */
    public List<String> getSourceMethodPrefixes() {
        return sourceMethodPrefixes;
    }

    /**
     * Sets the prefixes of source methods.
     * @param sourceMethodPrefixes the new prefixes.
     */
    public void setSourceMethodPrefixes(List<String> sourceMethodPrefixes) {
        this.sourceMethodPrefixes = sourceMethodPrefixes;
    }

    /**
     * Gets the sink method prefixes list
     * @return
     */
    public List<String> getSinkMethodPrefixes() {
        return sinkMethodPrefixes;
    }

    /**
     * Sets the prefixes of the sink methods.
     * @param sinkMethodPrefixes the new prefixes
     */
    public void setSinkMethodPrefixes(List<String> sinkMethodPrefixes) {
        this.sinkMethodPrefixes = sinkMethodPrefixes;
    }

    private HashMap<Unit, HashSet<Value>> lastTaintIn;

    /**
     * Returns the last taintIn set of the last analysis.
     * This is a workaround method.
     * @return
     */
    public HashMap<Unit, HashSet<Value>> getLastTaintIn() {
        return lastTaintIn;
    }


}
