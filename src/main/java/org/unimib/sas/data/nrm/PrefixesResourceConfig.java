package org.unimib.sas.data.nrm;

import com.esotericsoftware.yamlbeans.YamlReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Singleton class that manages the prefixes configurations of native resources.
 */
public class PrefixesResourceConfig extends AbstractResourceConfig {

    private static final Logger logger = LoggerFactory.getLogger(PrefixesResourceConfig.class.getName());

    private static final PrefixesResourceConfig instance = new PrefixesResourceConfig();

    private List<String> inputClasses;
    private List<String> targetClasses;
    private List<String> mixedClasses;
    private List<String> classNamePatterns;


    /**
     * Initializes the current instance using the data contained in the given yaml file.
     * @param fileName the file name of the yaml configuration file.
     * @return The initialiazed current instance.
     */
    @SuppressWarnings("unchecked")
    public static PrefixesResourceConfig createInstance(String fileName){
        synchronized (instance){
            try (FileReader fr = new FileReader(fileName)){
                YamlReader reader = new YamlReader(fr);
                Map prefixes = (Map)reader.read();
                instance.inputClasses = (List<String>)prefixes.get("input_classes");
                instance.targetClasses = (List<String>)prefixes.get("target_classes");
                instance.mixedClasses = (List<String>)prefixes.get("mixed_classes");
                instance.inputMethodPrefixes = (List<String>)prefixes.get("input_method_prefixes");
                instance.targetMethodPrefixes = (List<String>)prefixes.get("target_method_prefixes");
                instance.mixedMethodPrefixes = (List<String>)prefixes.get("mixed_method_prefixes");
                instance.classNamePatterns = (List<String>)prefixes.get("class_name_patterns");
                instance.inputSecondaryMethodPrefixes = (List<String>)prefixes.get("input_secondary_method_prefixes");
                instance.mixedSecondaryMethodPrefixes = (List<String>)prefixes.get("mixed_secondary_method_prefixes");
                instance.targetSecondaryMethodPrefixes = (List<String>)prefixes.get("target_secondary_method_prefixes");

                instance.inputAllMethodPrefixes = new ArrayList<>();
                instance.inputAllMethodPrefixes.addAll(instance.inputMethodPrefixes);
                instance.inputAllMethodPrefixes.addAll(instance.inputSecondaryMethodPrefixes);
                instance.mixedAllMethodPrefixes = new ArrayList<>();
                instance.mixedAllMethodPrefixes.addAll(instance.mixedMethodPrefixes);
                instance.mixedAllMethodPrefixes.addAll(instance.mixedSecondaryMethodPrefixes);
                instance.targetAllMethodPrefixes = new ArrayList<>();
                instance.targetAllMethodPrefixes.addAll(instance.targetMethodPrefixes);
                instance.targetAllMethodPrefixes.addAll(instance.targetSecondaryMethodPrefixes);
            }catch (IOException e){
                logger.error("Could not load the yaml prefixes file: {}", fileName);
            }
        }
        return instance;
    }

    /**
     * The current instance. Be sure you initialiazed this instance before using it, otherwise all lists are null.
     * @return The current PrefixesResourceConfig instance.
     */
    public static PrefixesResourceConfig getInstance(){
        return instance;
    }

    public List<String> getInputClasses() {
        return inputClasses;
    }

    public void setInputClasses(List<String> inputClasses) {
        this.inputClasses = inputClasses;
    }

    public List<String> getTargetClasses() {
        return targetClasses;
    }

    public void setTargetClasses(List<String> targetClasses) {
        this.targetClasses = targetClasses;
    }

    public List<String> getMixedClasses() {
        return mixedClasses;
    }

    public void setMixedClasses(List<String> mixedClasses) {
        this.mixedClasses = mixedClasses;
    }

    public List<String> getClassNamePatterns() {
        return classNamePatterns;
    }

    public void setClassNamePatterns(List<String> classNamePatterns) {
        this.classNamePatterns = classNamePatterns;
    }

    @Override
    public Map<String, List<String>> getClassNamePatternsCategories() {
        throw new UnsupportedOperationException("This operation is not supported by the current implementation.");
    }

    @Override
    public Map<String, List<String>> getNativeResourceCategories() {
        throw new UnsupportedOperationException("This operation is not supported by the current implementation.");
    }

    @Override
    public void setNativeResourceCategories(Map<String, List<String>> categories) {
        throw new UnsupportedOperationException("This operation is not supported by the current implementation.");
    }

    @Override
    public void setClassNamePatternsCategories(Map<String, List<String>> categories) {
        throw new UnsupportedOperationException("This operation is not supported by the current implementation.");
    }
}
