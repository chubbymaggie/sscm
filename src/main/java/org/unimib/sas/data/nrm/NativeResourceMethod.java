package org.unimib.sas.data.nrm;

import org.unimib.sas.data.IResourceManager;
import soot.SootMethod;

/**
 * Represents a native resource method with a given category.
 */
public class NativeResourceMethod {

    private SootMethod method;
    private IResourceManager.Category category;
    private String declaringClassCategory;

    public NativeResourceMethod(SootMethod method){
        assert method != null;
        this.method = method;
    }

    public NativeResourceMethod(SootMethod method, IResourceManager.Category category, String declaringClassCategory){
        this(method);
        this.category = category;
        this.declaringClassCategory = declaringClassCategory;
    }

    public void setCategory(IResourceManager.Category category) {
        this.category = category;
    }

    public IResourceManager.Category getCategory() {
        return category;
    }

    public SootMethod getMethod() {
        return method;
    }

    public String getDeclaringClassCategory() {
        return declaringClassCategory;
    }

    public void setDeclaringClassCategory(String declaringClassCategory) {
        this.declaringClassCategory = declaringClassCategory;
    }
}
