package org.unimib.sas.data.nrm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.utils.Prefixes;
import soot.*;

import java.util.*;

/**
 * Manages the native resource representation, allowing to check a given soot class or soot method if it's a NR.
 * The class uses the NRMPrefix singleton instance class to determine the category of each native resource.
 */
public class PrefixResourcesManager implements IResourceManager {
    private static final Logger logger = LoggerFactory.getLogger(PrefixResourcesManager.class.getName());
    public static final String DUMMY_NATIVE_RESOURCE_INTERFACE = "dummy.NativeResourceInterface";
    public static final String DUMMY_INPUT_NATIVE_RESOURCE_INTERFACE = "dummy.InputNativeResourceInterface";
    public static final String DUMMY_TARGET_NATIVE_RESOURCE_INTERFACE = "dummy.TargetNativeResourceInterface";
    public static final String DUMMY_MIXED_NATIVE_RESOURCE_INTERFACE = "dummy.MixedNativeResourceInterface";

    private PrefixesResourceConfig prefixes;
    private Map<SootMethod, NativeResourceMethod> nativeResourceMethods;
    private boolean useMethodDispatch;
    private boolean isInit = false;

    private Set<SootClass> sootNativeClasses;
    private SootClass dummyNativeResourceInterface;
    private SootClass dummyInputNativeResourceInterface;
    private SootClass dummyTargetNativeResourceInterface;
    private SootClass dummyMixedNativeResourceInterface;

    /**
     * Initializes a new manager.
     * @param useMethodDispatch True to use method dispatch, false otherwise.
     */
    public PrefixResourcesManager(boolean useMethodDispatch) {
        this.useMethodDispatch = useMethodDispatch;
        this.prefixes = PrefixesResourceConfig.getInstance();
        this.sootNativeClasses = new LinkedHashSet<>();
    }

    /**
      * Initializes a new manager which will use method dispatch to extract NRM.
     */
    public PrefixResourcesManager(){
        this(true);
    }

    /**
     * Initializes a new manager which will use the given prefixes file to extract NRM.
     * Dispatch methods is set to True.
     * @param prefixesYamlFile The yaml file.
     */
    public PrefixResourcesManager(String prefixesYamlFile){
        this(prefixesYamlFile, true);
    }

    /**
     * Initializes a new manager which will use the given prefixes file to extract NRM and will use or not a method dispatch.
     * @param prefixesYamlFile The yaml file.
     * @param useMethodDispatch True to use method dispatch, false otherwise.
     */
    public PrefixResourcesManager(String prefixesYamlFile, boolean useMethodDispatch){
        this.prefixes = PrefixesResourceConfig.createInstance(prefixesYamlFile);
        this.useMethodDispatch = useMethodDispatch;
        this.sootNativeClasses = new HashSet<>();
    }

    /**
     * Initialiazes the native resource collections and sets a dummy interface to every native resource soot class.
     */
    public void initNativeResources(){
        if (isInit)
            return;
        addNRMCategory(prefixes.getInputClasses(), IResourceManager.Category.Input);
        addNRMCategory(prefixes.getTargetClasses(), IResourceManager.Category.Target);
        addNRMCategory(prefixes.getMixedClasses(), IResourceManager.Category.Mixed);

        if (useMethodDispatch) {
            logger.info("A total of {} / {} NRMs were added using abstract dispatch.", dispatchCount, nativeResourceMethods.size());
            logger.info("While {} concrete dispatches failed (not so important).", dispatchErrors);
        }

        // Set a common interface for every native soot class
        if (!Scene.v().containsClass(DUMMY_NATIVE_RESOURCE_INTERFACE)) {
            SootClass objectClass = Scene.v().forceResolve("java.lang.Object",SootClass.BODIES);
            dummyNativeResourceInterface = new SootClass(DUMMY_NATIVE_RESOURCE_INTERFACE, Modifier.INTERFACE);
            dummyNativeResourceInterface.setInScene(true);
            dummyNativeResourceInterface.setSuperclass(objectClass);

            dummyInputNativeResourceInterface = new SootClass(DUMMY_INPUT_NATIVE_RESOURCE_INTERFACE, Modifier.INTERFACE);
            dummyInputNativeResourceInterface.setInScene(true);
            dummyInputNativeResourceInterface.setSuperclass(objectClass);
            dummyInputNativeResourceInterface.addInterface(dummyNativeResourceInterface);

            dummyTargetNativeResourceInterface = new SootClass(DUMMY_TARGET_NATIVE_RESOURCE_INTERFACE, Modifier.INTERFACE);
            dummyTargetNativeResourceInterface.setInScene(true);
            dummyTargetNativeResourceInterface.setSuperclass(objectClass);
            dummyTargetNativeResourceInterface.addInterface(dummyNativeResourceInterface);

            dummyMixedNativeResourceInterface = new SootClass(DUMMY_MIXED_NATIVE_RESOURCE_INTERFACE, Modifier.INTERFACE);
            dummyMixedNativeResourceInterface.setInScene(true);
            dummyMixedNativeResourceInterface.setSuperclass(objectClass);
            dummyMixedNativeResourceInterface.addInterface(dummyNativeResourceInterface);

        }else {
            dummyNativeResourceInterface = Scene.v().getSootClass(DUMMY_NATIVE_RESOURCE_INTERFACE);
            dummyInputNativeResourceInterface = Scene.v().getSootClass(DUMMY_INPUT_NATIVE_RESOURCE_INTERFACE);
            dummyTargetNativeResourceInterface = Scene.v().getSootClass(DUMMY_TARGET_NATIVE_RESOURCE_INTERFACE);
            dummyMixedNativeResourceInterface = Scene.v().getSootClass(DUMMY_MIXED_NATIVE_RESOURCE_INTERFACE);
        }

        assert dummyNativeResourceInterface.isInterface();
        Scene.v().releaseFastHierarchy();
        for (SootClass nrc : sootNativeClasses){
            boolean hasDummyInterface = false;
            for (SootClass interfaceOfNrc : nrc.getInterfaces())
                if (interfaceOfNrc.getName().equals(DUMMY_NATIVE_RESOURCE_INTERFACE))
                    hasDummyInterface = true;
            if (!hasDummyInterface)
                nrc.addInterface(dummyNativeResourceInterface);
        }
        Scene.v().releaseFastHierarchy(); // Reset the fast hierarchy in order
        Scene.v().getOrMakeFastHierarchy(); // to allow update
        Scene.v().releaseActiveHierarchy();
        Scene.v().getActiveHierarchy();
        isInit = true;
    }

    /**
     * Gets the native resource methods collection.
     * @return A map containing for each native resource (soot) method a NativeResourceMethod object,
     * containing its classification.
     */
    public Map<SootMethod, NativeResourceMethod> getNativeResourceMethods() {
        if (nativeResourceMethods.isEmpty()) {
            initNativeResources();
        }
        return nativeResourceMethods;
    }

    /**
     * Checks if the given method is contained in the native resource methods.
     * @param sm The method to check.
     * @return True if the method is a Native Resource MethodNode, false otherwise.
     */
    public boolean isNativeResourceMethod(SootMethod sm){
        return nativeResourceMethods.containsKey(sm);
    }

    /**
     * Gets the Native Resource Category of the given method.
     * @param sm The method to check.
     * @return The category of the method.
     */
    public String getNativeResourceCategory(SootMethod sm){
        if (isNativeResourceMethod(sm)){
            return nativeResourceMethods.get(sm).getCategory().toString();
        }else{
            return IResourceManager.Category.None.toString();
        }
    }

    /**
     * Checks if the given class is a Native Resource Class or implements/extends one.
     * @param sc The class to check
     * @return The native resource class that the given class implements/extends.
     */
    public SootClass isNativeResourceClass(SootClass sc){
        Hierarchy activeHierarchy = Scene.v().getActiveHierarchy();
        if (sc.isPhantom())
            return null;
        for (SootClass nrc : sootNativeClasses){
            if(nrc.isInterface()){
                if(activeHierarchy.getImplementersOf(nrc).contains(sc))
                    return nrc;
            }else if(activeHierarchy.isClassSuperclassOfIncluding(nrc, sc)){
                return nrc;
            }
        }
        return null;
    }

    /**
     * Gets the Native Resource Category of the given class.
     * @param sc The class to check.
     * @return The category of the class.
     */
    public String getNativeResourceCategory(SootClass sc){
        String name = sc.getName();
        if (prefixes.getInputClasses().contains(name))
            return IResourceManager.Category.Input.toString();
        else if (prefixes.getTargetClasses().contains(name))
            return IResourceManager.Category.Target.toString();
        else if (prefixes.getMixedClasses().contains(name))
            return IResourceManager.Category.Mixed.toString();
        else
            return IResourceManager.Category.None.toString();
    }

    int dispatchCount = 0;
    int dispatchErrors = 0;
    /**
     * Given a list of NR Classes, and their category, all the native resource methods are extracted from the given classes.
     * Every direct subclass of the given classes is scanned or every implementer of the given interfaces.
     * @param nativeClasses The list of native resource methods.
     * @param classCategory The category of the given classes (every class has the same category)
     * @return A map where keys are SootMethods and values are NativeResourceMethods objects, if a method
     * is in this map then it is a NRM.
     */
    private void addNRMCategory(Collection<String> nativeClasses, IResourceManager.Category classCategory){
        if (nativeResourceMethods == null)
            nativeResourceMethods = new HashMap<>();

        Hierarchy hierarchy = Scene.v().getActiveHierarchy();

        for (String className : nativeClasses){
            SootClass sootClass = Scene.v().forceResolve(className, SootClass.BODIES);
            if (sootClass == null) {
                logger.warn("Class {} is null class and was ignored as native resource.", className);
                continue;
            }else if (sootClass.isPhantom()){
                logger.warn("Class {} is phantom but was added as Native Resource.", className);
            }
            sootNativeClasses.add(sootClass);
            //nativeSootClasses.addAll(getAllDirectChildsOf(sootClass));
        }

        for (SootClass sootClass : sootNativeClasses){
            for (SootMethod sm : sootClass.getMethods()){
                if (sm.isConstructor())
                    continue;
                IResourceManager.Category methodCategory;
                if (Prefixes.startsWithAny(sm.getName(), prefixes.getInputMethodPrefixes())){
                    methodCategory = IResourceManager.Category.Input;
                } else if (Prefixes.startsWithAny(sm.getName(), prefixes.getTargetMethodPrefixes())){
                    methodCategory = IResourceManager.Category.Target;
                } else if (Prefixes.startsWithAny(sm.getName(), prefixes.getMixedMethodPrefixes())){
                    methodCategory = IResourceManager.Category.Mixed;
                } else {
                    methodCategory = IResourceManager.Category.None;
                }
                if (methodCategory != IResourceManager.Category.None) {
                    nativeResourceMethods.put(sm, new NativeResourceMethod(sm, methodCategory, classCategory.toString()));
                    // Add dispatched methods (overloads)
                    try {
                        if (useMethodDispatch && !sootClass.isConcrete()) {
                            Collection<SootMethod> dispatchMethods = hierarchy.resolveAbstractDispatch(sootClass, sm);
                            for (SootMethod dsm : dispatchMethods) {
                                if (!nativeResourceMethods.containsKey(dsm)) {
                                    nativeResourceMethods.put(dsm, new NativeResourceMethod(dsm, methodCategory, classCategory.toString()));
                                    dispatchCount++;
                                }
                            }
                        }
                    }catch (RuntimeException e){
                        dispatchErrors++;
                    }
                }
            }
        }
    }

    /**
     * Calculates all the direct childs of the given SootClass.
     * @param sc The SootClass.
     * @return A set containing all the direct childs of the given SootClass.
     */
    private Set<SootClass> getAllDirectChildsOf(SootClass sc){
        FastHierarchy hierarchy = Scene.v().getOrMakeFastHierarchy();
        Set<SootClass> childs = new HashSet<>();
        if (sc.isInterface()){
            childs.addAll(hierarchy.getAllImplementersOfInterface(sc));
        } else if (sc.isAbstract() || sc.isConcrete()){
            childs.addAll(hierarchy.getSubclassesOf(sc));
        }
        return childs;
    }

    public PrefixesResourceConfig getPrefixes() {
        return prefixes;
    }

    /**
     * Returns a dummy Interface that is extended/implemented by every native resource SootClass.
     * @return The SootClass object of the dummy native resource interface.
     */
    public SootClass getDummyNativeResourceInterface(){
        return dummyNativeResourceInterface;
    }

    public SootClass getDummyInputNativeResourceInterface() {
        return dummyInputNativeResourceInterface;
    }

    public SootClass getDummyTargetNativeResourceInterface() {
        return dummyTargetNativeResourceInterface;
    }

    public SootClass getDummyMixedNativeResourceInterface() {
        return dummyMixedNativeResourceInterface;
    }

    public boolean isUseMethodDispatch() {
        return useMethodDispatch;
    }

    /**
     * Retrieves the set of soot native classes created using the PrefixesResourceConfig object.
     * @return A set containing the soot native resource classes.
     */
    public Set<SootClass> getSootNativeClasses() {
        return sootNativeClasses;
    }

    /**
     * Returns the set of categories used by the current implementation.
     *
     * @return A set containing the categories used by the current implementation.
     */
    @Override
    public List<String> getCategories() {
       List<String> categories = new ArrayList<>();
        Arrays.stream(Category.values()).forEach(x-> categories.add(x.toString()));
        return Collections.unmodifiableList(categories);
    }
}
