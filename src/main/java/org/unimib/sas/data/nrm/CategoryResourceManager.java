package org.unimib.sas.data.nrm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.utils.Prefixes;
import soot.*;

import java.util.*;

/**
 * Implements a native resource manager using category based Native Resources.
 */
public class CategoryResourceManager implements IResourceManager {

    private final static Logger logger = LoggerFactory.getLogger(CategoryResourceManager.class.getName());

    private final static String DUMMY_PACKAGE = "dummy";
    private final static String DUMMY_NATIVE_RESOURCE_INTERFACE = "dummy.MainNativeResourceInterface";

    private boolean useMethodDispatch = true;
    private int dispatchCount = 0;
    private int dispatchErrors = 0;
    private boolean isInit = false;

    private CategoryResourceConfig nrConfig;
    private Map<SootMethod, NativeResourceMethod> nativeResourceMethods;
    private Map<String, SootClass> categoryInterface;
    private Map<String, Set<SootClass>> sootNativeClasses;

    public CategoryResourceManager(){
        this(true);
    }

    public CategoryResourceManager(String yamlFile){
        this(true);
        this.nrConfig = CategoryResourceConfig.createInstance(yamlFile);
    }

    public CategoryResourceManager(boolean useMethodDispatch){
        this.nrConfig = CategoryResourceConfig.getInstance();
        this.useMethodDispatch = useMethodDispatch;
        this.categoryInterface = new LinkedHashMap<>();
        this.sootNativeClasses = new LinkedHashMap<>();
    }

    /**
     * Initialiazes the native resource collections and sets a dummy interface to every native resource soot class.
     */
    public void initNativeResources(){
        if (isInit)
            return;
        initNativeResourceInternal();

        for (String category : nrConfig.getNativeResourceCategories().keySet()){
            initNativeResourceMethods(nrConfig.getNativeResourceCategories().get(category), category);
        }

        if (useMethodDispatch) {
            logger.info("A total of {} methods were added using abstract dispatch, for a total of {} NRMs.", dispatchCount, nativeResourceMethods.size());
            if (dispatchErrors > 0)
                logger.info("While {} concrete dispatches failed (not so important).", dispatchErrors);
        }
        isInit = true;
    }

    /**
     * Initialiazes the native resource interfaces and sets them a category interface.
     */
    private void initNativeResourceInternal() {
        Map<String, List<String>> nrcategories = nrConfig.getNativeResourceCategories();
        SootClass mainNRinterface = new SootClass(DUMMY_NATIVE_RESOURCE_INTERFACE, Modifier.INTERFACE);
        mainNRinterface.setSuperclass(Scene.v().forceResolve(Object.class.getName(), SootClass.BODIES));
        mainNRinterface.setInScene(true);
        for (String catName : nrcategories.keySet()){
            String className = ".".join(DUMMY_PACKAGE, catName + "NativeResourceInterface");
            SootClass dummyNativeResource;
            if (!Scene.v().containsClass(className)){
                dummyNativeResource = new SootClass(className, Modifier.INTERFACE);
                dummyNativeResource.addInterface(mainNRinterface);
                dummyNativeResource.setInScene(true);
            }else {
                dummyNativeResource = Scene.v().forceResolve(className, SootClass.BODIES);
            }
            categoryInterface.put(catName, dummyNativeResource);

            for (String nativeResourceClass : nrcategories.get(catName)){
                SootClass sc = Scene.v().forceResolve(nativeResourceClass, SootClass.BODIES);
                if (sc == null) {
                    logger.warn("A Native Resource was NULL because not found in the Scene: {}", nativeResourceClass);
                    continue;
                }
                if (sc.isPhantom())
                    logger.warn("A phantom Native Resource was detected but added anyway: {}", sc.getName());
                sc.addInterface(dummyNativeResource);
                sootNativeClasses.computeIfAbsent(catName, k -> new HashSet<>()).add(sc);
            }
        }

        Scene.v().releaseFastHierarchy(); // Reset the fast hierarchy in order
        Scene.v().getOrMakeFastHierarchy(); // to allow update
        Scene.v().releaseActiveHierarchy();
        Scene.v().getActiveHierarchy();
    }


    /**
     * Initialiazes the NRMs of the given classes using the given class category.
     * @param classes The Native Resources to search for NRMs
     * @param classCategory The Native Resources' category
     */
    private void initNativeResourceMethods(Collection<String> classes, String classCategory){
        if (nativeResourceMethods == null)
            nativeResourceMethods = new HashMap<>();

        Hierarchy hierarchy = Scene.v().getActiveHierarchy();

        for (String sootClassStr : classes){
            SootClass sootClass = Scene.v().forceResolve(sootClassStr, SootClass.BODIES);
            if (sootClass == null || sootClass.isPhantom())
                continue;
            for (SootMethod sm : sootClass.getMethods()){
                if (sm.isConstructor())
                    continue;
                IResourceManager.Category methodCategory;
                if (Prefixes.startsWithAny(sm.getName(), nrConfig.getInputMethodPrefixes())){
                    methodCategory = IResourceManager.Category.Input;
                } else if (Prefixes.startsWithAny(sm.getName(), nrConfig.getTargetMethodPrefixes())){
                    methodCategory = IResourceManager.Category.Target;
                } else if (Prefixes.startsWithAny(sm.getName(), nrConfig.getMixedMethodPrefixes())){
                    methodCategory = IResourceManager.Category.Mixed;
                } else {
                    methodCategory = IResourceManager.Category.None;
                }
                if (methodCategory != IResourceManager.Category.None) {
                    nativeResourceMethods.put(sm, new NativeResourceMethod(sm, methodCategory, classCategory));
                    // Add dispatched methods (overloads)
                    try {
                        if (useMethodDispatch && !sootClass.isConcrete()) {
                            Collection<SootMethod> dispatchMethods = hierarchy.resolveAbstractDispatch(sootClass, sm);
                            for (SootMethod dsm : dispatchMethods) {
                                if (!nativeResourceMethods.containsKey(dsm)) {
                                    nativeResourceMethods.put(dsm, new NativeResourceMethod(dsm, methodCategory, classCategory));
                                    dispatchCount++;
                                }
                            }
                        }
                    }catch (RuntimeException e){
                        dispatchErrors++;
                    }
                }
            }
        }
    }

    @Override
    public Map<SootMethod, NativeResourceMethod> getNativeResourceMethods() {
        return nativeResourceMethods;
    }

    @Override
    public boolean isNativeResourceMethod(SootMethod sm) {
        return nativeResourceMethods.containsKey(sm);
    }

    @Override
    public String getNativeResourceCategory(SootMethod sm) {
        if (nativeResourceMethods.containsKey(sm))
            return nativeResourceMethods.get(sm).getCategory().toString();
        return Category.None.toString();
    }

    @Override
    public SootClass isNativeResourceClass(SootClass sc) {
        if (!sc.implementsInterface(DUMMY_NATIVE_RESOURCE_INTERFACE))
            return null;
        for (String category : categoryInterface.keySet()){
            if (sc.implementsInterface(categoryInterface.get(category).getName())){
                return categoryInterface.get(category);
            }
        }
        return null;
    }

    @Override
    public String getNativeResourceCategory(SootClass sc) {
        if (!sc.implementsInterface(DUMMY_NATIVE_RESOURCE_INTERFACE))
            return Category.None.toString();
        for (String category : categoryInterface.keySet()){
            if (sc.implementsInterface(categoryInterface.get(category).getName())){
                return category;
            }
        }
        return Category.None.toString();
    }

    @Override
    public List<String> getCategories() {
        return Collections.unmodifiableList(new ArrayList<>(categoryInterface.keySet()));
    }

    /**
     * Returns the map containing for each category the Native Resources belonging to such category.
     * @return A map where keys represent Native Resource Categories while values are the set of Native
     * Resources for such category.
     */
    public Map<String, Set<SootClass>> getSootNativeClasses() {
        return sootNativeClasses;
    }

    public Map<String, SootClass> getCategoryInterface() {
        return categoryInterface;
    }
}
