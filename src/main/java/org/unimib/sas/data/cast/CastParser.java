package org.unimib.sas.data.cast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import soot.*;

import java.util.LinkedList;
import java.util.List;

public abstract class CastParser {

    private final static Logger logger = LoggerFactory.getLogger(CastParser.class.getName());

    protected SootMethod parseCastMethodSignature(String signature){
        signature = removeSquareBrackets(signature);
        signature = replaceConstructor(signature);
        String className = getClassName(signature);
        String methodName = getMethodName(signature);
        SootClass sc = Scene.v().forceResolve(className, SootClass.BODIES);
        SootMethod sm = null;
        String shortName = methodName.replaceAll("\\(.*?\\)", "");
        List<Type> types = getMethodTypes(methodName);
        try {
            if (sc != null && !sc.isPhantom()) {
                sm = sc.getMethod(shortName, types);
            }
        }catch (RuntimeException e){
            // If the search returned null, try finding the method in their superclasses/superinterfaces
            Hierarchy hierarchy = Scene.v().getActiveHierarchy();
            if (sc.isInterface()){
                for(SootClass superinterface : hierarchy.getSuperinterfacesOf(sc)){
                    try {
                        sm = superinterface.getMethod(shortName, types);
                        if (sm != null)
                            break;
                    }catch (RuntimeException r){}
                }
            }else {
                for(SootClass superclass : hierarchy.getSuperclassesOf(sc)){
                    try {
                        sm = superclass.getMethod(shortName, types);
                        if (sm != null)
                            break;
                    }catch (RuntimeException r){}
                }
            }
            if (sm == null)
                logger.warn("Could not retrieve method {} because method was not found in the Scene.", signature);
        }
        return sm;
    }

    protected String getClassName(String str){
        String className = str.substring(0, str.lastIndexOf('('));
        className = className.substring(0, className.lastIndexOf('.'));
        return className;
    }

    protected String getMethodName(String str){
        String methodName = str.substring(0, str.lastIndexOf('('));
        methodName = str.substring(methodName.lastIndexOf('.') + 1,  str.lastIndexOf(')') + 1);
        return methodName;
    }

    protected String removeSquareBrackets(String str){
        return str.replaceAll("\\[[\\.a-zA-Z0-9\\+_\\-]+?\\]", "");
    }

    protected String replaceConstructor(String str){
        return str.replace("+ctor", SootMethod.constructorName);
    }

    protected List<Type> getMethodTypes(String method){
        List<Type> types = new LinkedList<>();
        try {
            method = method.substring(method.indexOf('('));
            String[] strTypes = method.replaceAll("[\\(|\\)]", "").split(",");
            for (String t : strTypes) {
                if (t.isEmpty())
                    continue;
                types.add(Scene.v().getType(t));
            }
        }catch (RuntimeException e){
        }
        return types;
    }
}
