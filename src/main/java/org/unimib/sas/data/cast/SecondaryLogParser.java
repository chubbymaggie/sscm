package org.unimib.sas.data.cast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Parses a secondary log file and loads the SootMethods from it.
 */
public class SecondaryLogParser extends CastParser {

    public static SecondaryLogParser instance;

    private final static Pattern LINE_PATTERN = Pattern.compile("No implementation found for method \".*\".*");
    private final static Pattern METHOD_PATTERN = Pattern.compile("\".*\"");

    private final static Logger logger = LoggerFactory.getLogger(SecondaryLogParser.class.getName());

    private HashSet<String> methodSignatures;
    private HashSet<SootMethod> methods;
    private HashSet<String> sootMethodSignatures;
    private File log;

    /**
     * Builds a new secondary log parser and checks for file existance using asserts.
     * @param logPath The path of the secondary log.
     */
    public static SecondaryLogParser getInstance(String logPath){
        if (instance == null)
            instance = new SecondaryLogParser(logPath);
        return instance;
    }

    /**
     * Builds a new secondary log parser and checks for file existance using asserts.
     * @param logPath The path of the secondary log.
     */
    private SecondaryLogParser(String logPath){
        this.log = new File(logPath);

        assert this.log.exists();
        assert this.log.isFile();
        assert this.log.canRead();

        this.methods = new HashSet<>();
        this.methodSignatures = new HashSet<>();
        this.sootMethodSignatures = new HashSet<>();
    }

    /**
     * Parses the file and builds the set of SootMethods that can be retrieved using getMethods()
     */
    public void parse(){
        reset();
        try(Scanner scanner = new Scanner(log)){
            while(scanner.hasNextLine()){
                String line = scanner.nextLine();
                if (!LINE_PATTERN.matcher(line).matches())
                    continue;

                line = line.substring(line.indexOf('"'), line.lastIndexOf('"')+1);
                if (METHOD_PATTERN.matcher(line).matches()){
                    String methodString = line.replaceAll("\"", "");
                    if (methodSignatures.contains(methodString)) // skip methods already checked for presence in the scene
                        continue;
                    methodSignatures.add(methodString);
                    SootMethod sm = parseCastMethodSignature(methodString);
                    if (sm == null){
                        logger.warn("Secondary log method {} not found in the Scene.", methodString);
                    }else{
                        methods.add(sm);
                        sootMethodSignatures.add(sm.getSignature());
                    }
                }
            }
            scanner.close();
            if (methods.isEmpty())
                logger.warn("No methods parsed from secondary log.");
        }catch (FileNotFoundException e){
            logger.error("Could not find secondary log file {}.", log.getAbsoluteFile());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Returns the set of methods found during the parsing.
     * @return The SootMethods parsed from the secondary log.
     */
    public HashSet<SootMethod> getMethods(){
        return methods;
    }

    public HashSet<String> getMethodsSignatures() {
        HashSet<String> signatures = new HashSet<>();
        for (SootMethod sm : methods){
            signatures.add(sm.getSignature());
        }
        return signatures;
    }

    /**
     * Returns the methods signatures of the SootMethods parsed from the secondary log, even if the methods are no more available in the Scene.
     * @return The set of soot signatures of the methods parsed.
     */
    public HashSet<String> getSootMethodSignatures() {
        return sootMethodSignatures;
    }

    public void reset(){
        methods.clear();
        methodSignatures.clear();
        sootMethodSignatures.clear();
    }


}
