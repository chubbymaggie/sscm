package org.unimib.sas.data.featureset;

import org.unimib.sas.data.IFeatureSet;
import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.data.nrm.CategoryResourceManager;
import org.unimib.sas.data.nrm.PrefixResourcesManager;

/**
 * Types of features sets.
 */
public enum FeatureSet {
    Naive("naive"),
    Prefix("prefix"),
    NoIndividualPrefixes("noindividualprefixes"),
    Aggregated("aggregated"),
    Category("category");

    private String value;
    FeatureSet(String value){
        this.value = value;
    }

    /**
     * Builds a new FeatureSet enum from string. Case is ignored.
     * @param value the value from which to build the enum.
     * @return The enumeration value of the string.
     */
    public static FeatureSet fromString(String value){
        for (FeatureSet fs : FeatureSet.values()){
            if (value.equalsIgnoreCase(fs.value))
                return fs;
        }
        throw new IllegalArgumentException("FeatureSet enumeration does not support: " + value);
    }

    /**
     * Returns an instance of the IFeatureSet that this enum value represents.
     * @param manager The manager to use for building the IFeatureSet
     * @return The feature set object.
     */
    public IFeatureSet toIFeatureSet(IResourceManager manager){
        IFeatureSet featureSet;
        switch (this) {
            case Prefix:
                featureSet = new PrefixFeatureSet((PrefixResourcesManager)manager);
                break;
            case NoIndividualPrefixes:
                featureSet = new NoIndividualPrefixesFeatureSet((PrefixResourcesManager)manager);
                break;
            case Aggregated:
                featureSet = new AggregatedFeatureSet((PrefixResourcesManager)manager);
                break;
            case Category:
                featureSet = new CategoryFeatureSet((CategoryResourceManager) manager);
                break;
            case Naive:
                featureSet = new NaiveFeatureSet((PrefixResourcesManager)manager);
                break;
            default:
                featureSet = new NaiveFeatureSet((PrefixResourcesManager)manager);
        }
        return featureSet;
    }
}
