package org.unimib.sas.data.featureset;

import org.unimib.sas.data.IFeature;
import org.unimib.sas.data.feature.*;
import org.unimib.sas.data.nrm.PrefixesResourceConfig;
import org.unimib.sas.data.nrm.PrefixResourcesManager;
import soot.*;

import java.util.*;
import java.util.AbstractMap.SimpleEntry;

/**
 * The first naive implementation of the feature set.
 */
public class NaiveFeatureSet extends AbstractFeatureSet {

    private PrefixResourcesManager nativeResourcesManager;

    public NaiveFeatureSet(PrefixResourcesManager nativeResourcesManager){
        super(nativeResourcesManager);
        this.nativeResourcesManager = nativeResourcesManager;
    }

    /**
     * Returns an ordered list of the features.
     *
     * @return A list where each item is an initialiazed IFeature and ready to be used to compute the feature for
     * the given method. The list is unmodifiable.
     */
    @Override
    public List<IFeature> getFeatureList() {
        if (!featureList.isEmpty()) {
            return Collections.unmodifiableList(featureList);
        }
        PrefixesResourceConfig prefixes = nativeResourcesManager.getPrefixes();

        // MethodNode Prefix features
        createMethodNamePrefixFeatures(prefixes.getInputMethodPrefixes(), "InMethodPrefixFeature");
        createMethodNamePrefixFeatures(prefixes.getTargetMethodPrefixes(), "TaMethodPrefixFeature");
        createMethodNamePrefixFeatures(prefixes.getMixedMethodPrefixes(), "MixMethodPrefixFeature");

        // Class name contains features
        createClassNameContainsFeatures(prefixes.getClassNamePatterns(), "ClassNameContains");

        // Has more than 0 params feature
        featureList.add(new HasParametersFeature("HasMore0ParametersFeature", nativeResourcesManager, 0));

        // Parameters are of given type or of its subtypes.
        createTypeParametersFeatures(prefixes.getInputClasses(), "InputParam", true);
        createTypeParametersFeatures(prefixes.getTargetClasses(), "TargetParam", true);
        createTypeParametersFeatures(prefixes.getMixedClasses(), "MixedParam", true);
        featureList.add(new TypeParametersFeature("StringParam", nativeResourcesManager, RefType.v("java.lang.String")));
        featureList.add(new TypeParametersFeature("String[]Param", nativeResourcesManager, RefType.v("java.lang.String").makeArrayType()));
        featureList.add(new TypeParametersFeature("Byte[]Param", nativeResourcesManager, ArrayType.v(ByteType.v(), 1)));
        featureList.add(new TypeParametersFeature("Char[]Param", nativeResourcesManager, ArrayType.v(CharType.v(), 1)));

        // Return type features
        List<Map.Entry<String, Type>> retTypes = new ArrayList<>();
        retTypes.add(new SimpleEntry<>("StringReturnType", RefType.v("java.lang.String")));
        retTypes.add(new SimpleEntry<>("VoidReturnType", VoidType.v()));
        retTypes.add(new SimpleEntry<>("String[]ReturnType", ArrayType.v(RefType.v("java.lang.String"),1)));
        retTypes.add(new SimpleEntry<>("Byte[]ReturnType", ArrayType.v(ByteType.v(), 1)));

        for (Map.Entry<String, Type> e : retTypes){
            featureList.add(new ReturnTypeFeature(e.getKey(), nativeResourcesManager, e.getValue(), false));
        }
        // Return type of native resource?
        featureList.add(new ReturnTypeFeature(
                "NativeResReturnType",
                nativeResourcesManager,
                RefType.v(nativeResourcesManager.getDummyNativeResourceInterface()), true));

        // Invokes a NRM that starts with the given prefixes?
        createNRMNamePrefixFeatures(prefixes.getInputMethodPrefixes(), "InvokesNRMPrefix");
        createNRMNamePrefixFeatures(prefixes.getTargetMethodPrefixes(), "InvokesNRMPrefix");
        createNRMNamePrefixFeatures(prefixes.getMixedMethodPrefixes(), "InvokesNRMPrefix");

        // Dataflow features
        featureList.add(new DataflowFromNRMToThis("DFfromNRMtoThis", nativeResourcesManager));
        featureList.add(new DataflowFromNRMToReturn("DFfromNRMtoRet", nativeResourcesManager));
        featureList.add(new DataflowFromParamToNRM("DFfromParamToNRM", nativeResourcesManager));
        featureList.add(new DataflowFromParamToThis("DFfromParamToThis", nativeResourcesManager));
        featureList.add(new DataflowFromNRMtoParam("DFfromNRMtoParam", nativeResourcesManager));

        return Collections.unmodifiableList(featureList);
    }


}
