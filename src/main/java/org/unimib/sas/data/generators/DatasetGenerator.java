package org.unimib.sas.data.generators;

import org.unimib.sas.data.*;
import soot.SootClass;
import soot.SootMethod;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Generates the dataset using the given predicate to filter the classes to analyse.
 */
public class DatasetGenerator implements IDatasetGenerator<IDataset> {

    private IFeatureSet featureSet;
    private Dataset dataset;
    private Predicate<SootClass> classFilter;
    private Predicate<SootMethod> methodFilter;

    /**
     * Creates the dataset using the given predicate to filter classes.
     * @param featureSet The feature set.
     * @param classFilter The predicate to use. If the predicate evaluates True then the item is used,
     *                        if it evaluates False, the item will NOT be used.
     */
    public DatasetGenerator(IFeatureSet featureSet, Predicate<SootClass> classFilter, Predicate<SootMethod> methodFilter) {
        this.dataset = new Dataset(featureSet);
        this.featureSet = featureSet;
        this.classFilter = classFilter;
        this.methodFilter = methodFilter;
    }

    /**
     * Creates the dataset using all the classes, without filtering
     * @param featureSet The feature set.
     */
    public DatasetGenerator(IFeatureSet featureSet){
        this(featureSet, x -> true, x -> true);
    }

    /**
     * Calculates the features on the given soot classes according to the filters.
     * @param sootClasses The classes to analyze.
     */
    public void generate(Collection<SootClass> sootClasses){
        SootClass[] sootClassesArray = sootClasses.toArray(new SootClass[]{});
        for (int i = 0; i < sootClassesArray.length; i++){
            SootClass sc = sootClassesArray[i];
            if (classFilter.test(sc)){
                SootMethod[] methodsArray = sc.getMethods().toArray(new SootMethod[]{});
               for (int j = 0; j < methodsArray.length; j++){
                   SootMethod sm = methodsArray[j];
                   if (methodFilter.test(sm)) {
                       for (IFeature f : featureSet.getFeatureList()) {
                           dataset.setValue(sm, f.getFeatureName(), f.apply(sm));
                       }
                   }
                }
            }
        }
    }

    /**
     * Given a dataset with no features in it, calculates them in place on the given dataset.
     * @param dataset the dataset to calculate the features on.
     */
    public void generate(IDataset dataset){
        for (SootMethod sm : dataset.keep(methodFilter)){
            for (IFeature f : featureSet.getFeatureList()){
                dataset.setValue(sm, f.getFeatureName(), String.valueOf(f.apply(sm).getValue()));
            }
        }
    }

    /**
     * Returns the IDataset resulting from generation.
     * @return the IDataset resulting from generation.
     */
    public IDataset getDataset() {
        return dataset;
    }

    public IFeatureSet getFeatureSet() {
        return featureSet;
    }
}
