package org.unimib.sas.data;

import java.util.List;

/**
 * Represents a set of features objects that can be used to apply features in the same order for each method.
 */
public interface IFeatureSet {
    /**
     * Returns an ordered list of the features.
     * @return A list where each item is an initialiazed IFeature and ready to be used to compute the feature for
     * the given method.
     */
    List<IFeature> getFeatureList();

    /**
     * Returns all the names of the features in the same order of the
     * underlying feature list (the same order of calculation).
     * @return A list of strings, each is the name of a feature.
     */
    List<String> getFeatureNames();

}
