package org.unimib.sas.data.classification;

import com.google.common.collect.BiMap;
import org.dmg.pmml.Entity;
import org.dmg.pmml.FieldName;
import org.dmg.pmml.PMML;
import org.jpmml.evaluator.*;
import org.jpmml.model.PMMLUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unimib.sas.data.IDataset;
import org.xml.sax.SAXException;
import soot.SootMethod;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Classifier that uses a PMML file to load a model that will be used to classifie the dataset.
 */
public class PMMLClassification implements IClassification<IDataset> {

    private final static Logger logger = LoggerFactory.getLogger(PMMLClassification.class.getName());

    private Evaluator evaluator;
    private boolean verbose;

    public PMMLClassification(String file){
        File modelFile = new File(file);

        PMML model;
        try (FileInputStream fis = new FileInputStream(modelFile)){
            model = PMMLUtil.unmarshal(fis);
            ModelEvaluatorFactory modelEvaluatorFactory = ModelEvaluatorFactory.newInstance();
            this.evaluator = modelEvaluatorFactory.newModelEvaluator(model);
        }catch (SAXException | JAXBException | IOException e) {
            logger.error("Error while loading PMML model file: {}", e.getMessage());
            System.exit(0);
        }
        verbose = true;
    }


    /**
     * Predicts the classes of the given dataset
     *
     * @param dataset the dataset to classify
     * @return The new dataset containing the additional classification column(s).
     */
    @Override
    public IDataset predict(IDataset dataset) {
        logger.info("Predicting dataset using PMML model: {}", evaluator.getSummary());
        for (SootMethod sm : dataset){
            prepareArgumentsFor(sm, dataset);
            Map<FieldName, ?> results = evaluator.evaluate(arguments);
            for (TargetField targetField : evaluator.getTargetFields()){
                FieldName targetFieldName = targetField.getName();
                Object targetFieldValue = results.get(targetFieldName);

                if (targetFieldValue instanceof Computable){
                    Computable computable = (Computable)targetFieldValue;

                    dataset.setValue(sm, IDataset.PREDICTED, computable.getResult().toString());

                    // Check for probability...
                    if(targetFieldValue instanceof HasEntityId){
                        HasEntityId hasEntityId = (HasEntityId)targetFieldValue;
                        HasEntityRegistry<?> hasEntityRegistry = (HasEntityRegistry<?>)evaluator;
                        BiMap<String, ? extends Entity> entities = hasEntityRegistry.getEntityRegistry();
                        Entity winner = entities.get(hasEntityId.getEntityId());

                        // Test for "probability" result feature
                        if(targetFieldValue instanceof HasProbability){
                            HasProbability hasProbability = (HasProbability)targetFieldValue;
                            Double winnerProbability = hasProbability.getProbability(winner.getId());
                            dataset.setValue(sm, "winnerProb", winnerProbability.toString());
                        }
                    }
                }else {
                    // Target value is a primitive wrapper
                    dataset.setValue(sm, IDataset.PREDICTED, targetFieldValue.toString());
                }
                if (verbose)
                    logger.info("{} method was tagged as {}.", sm.getSignature(), dataset.getValue(sm, IDataset.PREDICTED));
            }
        }

        return dataset;
    }

    /**
     * Trains a model on the given dataset.
     *
     * @param dataset The dataset used for training.
     */
    @Override
    public void train(IDataset dataset) {
        throw new UnsupportedOperationException("Operation not supported by PMML classifier.");
    }

    private Map<FieldName, FieldValue> arguments = new LinkedHashMap<>();
    private void prepareArgumentsFor(SootMethod sm, IDataset dataset){
        arguments.clear();

        List<InputField> inputFields = evaluator.getInputFields();
        for(InputField inputField : inputFields){
            FieldName inputFieldName = inputField.getName();

            // The raw (ie. user-supplied) value could be any Java primitive value
            double rawValue = Double.parseDouble(dataset.getValue(sm, inputFieldName.getValue()));

            // The raw value is passed through: 1) outlier treatment, 2) missing value treatment, 3) invalid value treatment and 4) type conversion
            FieldValue inputFieldValue = inputField.prepare(rawValue);

            arguments.put(inputFieldName, inputFieldValue);
        }
    }

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }
}
