package org.unimib.sas.data.classification;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unimib.sas.data.IDataset;
import weka.attributeSelection.ASEvaluation;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.NominalPrediction;
import weka.classifiers.evaluation.Prediction;
import weka.classifiers.evaluation.output.prediction.AbstractOutput;
import weka.classifiers.evaluation.output.prediction.CSV;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.*;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.Remove;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.function.Function;

/**
 * Instantiates a Weka classifier.
 */
public class WekaClassification implements IClassification<Instances>, Serializable {

    private final static Logger logger = LoggerFactory.getLogger(WekaClassification.class.getName());

    private Classifier classifier;
    private String classifierName;
    private String[] classifierArgs;
    private Random random = new Random(1);
    private boolean verbose = true;
    private List<String> attributeClasses;

    /**
     * Instantiates a weka classifier.
     * @param wekaClassifierClassname the classifier to instantiates. Indicate full class name.
     * @param classifierOptions the option to pass to the classifier.
     */
    public WekaClassification(String wekaClassifierClassname, String... classifierOptions) {
        this.classifierName = wekaClassifierClassname;
        this.classifierArgs = classifierOptions;
    }

    /**
     * Instantiates a default SMO weka model using tested settings with the default Category-based dataset.
     */
    public WekaClassification(){
        this("weka.classifiers.functions.SMO",
                "-C", "0.35", "-K", "weka.classifiers.functions.supportVector.PolyKernel -E 3 -L");
    }

    @Override
    public Instances predict(Instances dataset) {
        if (classifier == null){
            throw new RuntimeException("Classifier was not instantiated.");
        }
        int classAttrIndex = 1;
        Attribute classAttribute = new Attribute(IDataset.TAG, this.attributeClasses);
        dataset.insertAttributeAt(classAttribute, classAttrIndex);
        dataset.setClassIndex(classAttrIndex);

        Instances labeledDataset = new Instances(dataset);
        try{
            for (int i = 0; i < dataset.size(); i++){
                double classification = classifier.classifyInstance(labeledDataset.get(i));
                labeledDataset.get(i).setClassValue(classification);
                if (verbose)
                    logger.info("{} -> {}", labeledDataset.get(i).stringValue(0),
                            labeledDataset.get(i).classAttribute().value((int)classification));
            }
        }catch (Exception e){
            logger.error("Unhandled error during prediction:\n{}", e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }

        return labeledDataset;
    }

    @Override
    public void train(Instances dataset) {
        try {
            Classifier evalClassifier = toFilteredClassifier(getClassifier(), dataset.attribute(IDataset.METHOD_NAME).index());
            Evaluation evaluator = new Evaluation(dataset);
            int folds = 10;
            if (verbose) {
                System.out.printf("Model %s is being evaluated with %s-fold cross-validation.\n", classifierName, folds);
            }

            dataset.setClass(dataset.attribute(IDataset.TAG));
            evaluator.crossValidateModel(evalClassifier, dataset, folds, random);

            this.attributeClasses = new ArrayList<>();
            for (Enumeration<Object> e = dataset.classAttribute().enumerateValues(); e.hasMoreElements();)
                attributeClasses.add((String)e.nextElement());

            if (verbose){
                System.out.printf("Model %s evaluation results:\n", classifierName);
                System.out.println(evaluator.toSummaryString());
                System.out.println(evaluator.toClassDetailsString());
                System.out.println(evaluator.toMatrixString());
            }
            classifier = toFilteredClassifier(getClassifier(), dataset.attribute(IDataset.METHOD_NAME).index());
            classifier.buildClassifier(dataset);
        }catch (Exception e){
            logger.error("Unhandled WEKA error during training:\n{}", e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
    }


    private Classifier toFilteredClassifier(Classifier classifier, int attrIndexToRemove){
        Remove rm = new Remove();
        rm.setAttributeIndices(String.valueOf(attrIndexToRemove+1));
        FilteredClassifier filteredClassifier = new FilteredClassifier();
        filteredClassifier.setFilter(rm);
        filteredClassifier.setClassifier(classifier);
        return filteredClassifier;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    private Classifier getClassifier() throws Exception{
        return AbstractClassifier.forName(classifierName, classifierArgs);
    }

    public String getClassifierName() {
        return classifierName;
    }

    /**
     * Reads a WekaClassification model from file.
     * @param file the file stream containing the model.
     * @return the WekaClassification object contained in the file.
     * @throws Exception The exception thrown if something went wrong.
     */
    public static WekaClassification load(FileInputStream file) throws Exception{
        return (WekaClassification)SerializationHelper.read(file);
    }

    /**
     * Loads the WekaClassification model saved on the given file.
     * @param file the file to load the model from.
     * @return The classifier object.
     * @throws Exception Usually thrown if Weka fails loading the model.
     */
    public static WekaClassification load(String file) throws Exception{
        Object cls;
        try {
            cls = SerializationHelper.read(file);
        }catch (Exception e){
            logger.error("An unexpected error occurred while loading the classifier model.");
            logger.error("Make sure you use the same class you used for serialization.");
            throw e;
        }

        return (WekaClassification)cls;
    }

    /**
     * Serialiazes a WekaClassification to a file.
     * @param obj the object to write.
     * @param file the file to write the object to.
     * @throws Exception The exception thrown if something went wrong.
     */
    public static void serialize(WekaClassification obj, FileOutputStream file) throws Exception{
        SerializationHelper.write(file, obj);
    }
}
