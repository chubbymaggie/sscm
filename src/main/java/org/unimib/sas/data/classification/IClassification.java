package org.unimib.sas.data.classification;

import org.unimib.sas.data.IDataset;

/**
 * Models a classification in bulk of a certain dataset type T.
 * @param <T>
 */
public interface IClassification<T> {
    /**
     * Predicts the classes of the given dataset
     * @param dataset the dataset to classify
     * @return The new dataset containing the additional classification column(s).
     */
    T predict(T dataset);

    /**
     * Trains a model on the given dataset.
     * @param dataset The dataset used for training.
     */
    void train(T dataset);

    boolean isVerbose();
    void setVerbose(boolean verbose);
}
