package org.unimib.sas.data.classification;

public enum ClassificationResult {
    Input("Input"),
    Target("Target"),
    Mixed("Mixed"),
    None("None");

    private String text;

    ClassificationResult(String text){
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return getText();
    }

    public static ClassificationResult fromString(String text){
        for (ClassificationResult r : ClassificationResult.values()){
            if (r.text.equalsIgnoreCase(text))
                return r;
        }
        return null;
    }
}
