package org.unimib.sas.data.classification;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.unimib.sas.data.IDataset;
import org.unimib.sas.data.IPersistency;

import java.io.IOException;

/**
 * Models a classifier that uses an external process for classification and exchanges data using a IPersistency object.
 */
public class PythonWrapperClassification implements IClassification<IDataset> {

    private static final Logger logger = LoggerFactory.getLogger(PythonWrapperClassification.class.getName());

    private IPersistency data;
    private String classifierCommand;
    private String modelFileName;
    private boolean verbose;

    /**
     * The IPersistency object to use for the classification tasks
     * @param data The data
     * @param classifierCommand the classifier's process filepath to execute.
     * @param modelFileName the model file to use for classifications
     */
    public PythonWrapperClassification(IPersistency data, String classifierCommand, String modelFileName){
        this.data = data;
        this.classifierCommand = classifierCommand;
        this.modelFileName = modelFileName;
        this.verbose = true;
    }

    @Override
    public IDataset predict(IDataset dataset) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        IDataset result = null;
        try {
            data.save(dataset);
            processBuilder.command(classifierCommand, "--model_file", modelFileName,
                    "predict", data.getFile().getCanonicalPath(), data.getFile().getCanonicalPath());
            logger.info("Classifying the dataset...");
            processBuilder.inheritIO();
            Process classifier = processBuilder.start();
            classifier.waitFor();
            if(classifier.exitValue() != 0) {
                logger.error("Classification failed, the python classifier returned with non-zero error code {}", classifier.exitValue());
                return null;
            }
            result = data.load();
        }catch (IOException e){
            logger.error("Could not predict the given data using the given model: {}", e.getMessage());
        }catch (InterruptedException e){
            logger.error("The process waiting for classification was interrupted.");
        }
        return result;
    }

    @Override
    public void train(IDataset dataset) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        try {
            data.save(dataset);
            processBuilder.command(classifierCommand, "--model_file", modelFileName,
                    "train", data.getFile().getCanonicalPath(), data.getFile().getCanonicalPath());
            logger.info("The classifier is being trained using the given dataset...");
            processBuilder.inheritIO();
            Process classifier = processBuilder.start();
            classifier.waitFor();
        }catch (IOException e){
            logger.error("Could not train the given model on the given dataset: {}", e.getMessage());
        }catch (InterruptedException e){
            logger.error("The process waiting for training was interrupted.");
        }
    }

    public IPersistency getData() {
        return data;
    }

    @Override
    public boolean isVerbose() {
        return false;
    }

    @Override
    public void setVerbose(boolean verbose) {

    }
}
