package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.FastHierarchy;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;

import java.util.Set;

/**
 * Checks if the method's declaring class is a subtype, in any way, of the given parent soot class.
 * The parent soot class can be an interface. In this case if the declaring class is an interface too it is checked if
 * the declaring class is a subinterface of the parent. If the declaring class is a class it is checked if it implements the
 * parent interface.
 * If the parent is not an interface it is checked if the declaring class is a subclass of the parent.
 */
public class DeclaringClassIsSubtypeOfFeature extends AbstractFeature {

    private FastHierarchy fastHierarchy;
    private Set<SootClass> cachedSubInterfaces;
    private Set<SootClass> cachedImplementers;
    private SootClass parent;

    public DeclaringClassIsSubtypeOfFeature(String featureName, IResourceManager nrManager, SootClass parent) {
        super(featureName, nrManager);
        this.parent = parent;
        this.fastHierarchy = Scene.v().getOrMakeFastHierarchy();

        this.cachedSubInterfaces = fastHierarchy.getAllSubinterfaces(parent);
        this.cachedImplementers = fastHierarchy.getAllImplementersOfInterface(parent);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm))
            return FeatureResult.UNDEFINED;

        SootClass declaringClass = sm.getDeclaringClass();
        if (parent.isInterface()){
            if (declaringClass.isInterface()){
                return cachedSubInterfaces.contains(declaringClass) ? FeatureResult.TRUE : FeatureResult.FALSE;
            }else{
                return cachedImplementers.contains(declaringClass) ? FeatureResult.TRUE : FeatureResult.FALSE;
            }
        } else {
          if (declaringClass.isInterface()){
              return FeatureResult.FALSE;
          } else {
              return fastHierarchy.isSubclass(declaringClass, parent) ? FeatureResult.TRUE : FeatureResult.FALSE;
          }
        }
    }
}
