package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;

public abstract class NameContainsFeature extends AbstractFeature {

    protected String pattern;

    public NameContainsFeature(String featureName, IResourceManager nrManager, String pattern) {
        super(featureName, nrManager);
        this.pattern = pattern.toLowerCase();
    }

}
