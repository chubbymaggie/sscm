package org.unimib.sas.data.feature;

/**
 * Represents the result of an extended boolean feature.
 * The ILLEGAL type is used when the feature result is not valid at all, while the UNDEFINED
 * is used when the feature calculation is not defined for the given method.
 */
public enum FeatureResult {
    ILLEGAL(-1),
    UNDEFINED(0),
    FALSE(1),
    TRUE(2);

    private final int value;

    FeatureResult(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static FeatureResult fromInt(int value){
        for (FeatureResult r : FeatureResult.values()){
            if(r.getValue() == value)
                return r;
        }
        return ILLEGAL;
    }

    @Override
    public String toString() {
        return String.valueOf(getValue());
    }
}
