package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;

public abstract class ParametersFeature extends AbstractFeature{
    public ParametersFeature(String featureName, IResourceManager nrManager) {
        super(featureName, nrManager);
    }
}
