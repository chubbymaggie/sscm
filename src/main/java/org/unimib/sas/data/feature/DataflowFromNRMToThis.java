package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.*;
import soot.jimple.InvokeExpr;

import java.util.HashMap;
import java.util.HashSet;

public class DataflowFromNRMToThis extends DataflowFeature {
    public DataflowFromNRMToThis(String featureName, IResourceManager nrManager) {
        super(featureName, nrManager);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm) || sm.isStatic())
            return FeatureResult.UNDEFINED;

        Body body = sm.retrieveActiveBody();
        HashSet<InvokeExpr> nrmStatements = extractNativeResourceMethodStatements(body);
        HashMap<Unit, HashSet<Value>> taintOut = getTaintAnalyzer().taintAnalysisMethodCalls(body, nrmStatements);

        Local thisLocal = body.getThisLocal();
        for (HashSet<Value> unitTaintOut : taintOut.values()){
            if (unitTaintOut.contains(thisLocal))
                return FeatureResult.TRUE;
        }

        return FeatureResult.FALSE;
    }
}
