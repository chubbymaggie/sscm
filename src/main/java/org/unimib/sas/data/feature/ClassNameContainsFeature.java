package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.SootClass;
import soot.SootMethod;

public class ClassNameContainsFeature extends NameContainsFeature {
    public ClassNameContainsFeature(String featureName, IResourceManager nrManager, String pattern) {
        super(featureName, nrManager, pattern);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm))
            return FeatureResult.UNDEFINED;
        SootClass sc = sm.getDeclaringClass();
        if (sc.getName().toLowerCase().contains(pattern))
            return FeatureResult.TRUE;
        return FeatureResult.FALSE;
    }
}
