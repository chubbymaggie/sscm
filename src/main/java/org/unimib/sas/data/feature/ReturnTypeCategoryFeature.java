package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.data.nrm.PrefixResourcesManager;
import soot.RefType;
import soot.SootClass;
import soot.SootMethod;

/**
 * Checks if the return type of the method matches the given {@link IResourceManager.Category}.
 */
public class ReturnTypeCategoryFeature extends AbstractFeature {

    private String category;

    public ReturnTypeCategoryFeature(String featureName, IResourceManager nrManager, String category) {
        super(featureName, nrManager);
        this.category = category;
    }

    public ReturnTypeCategoryFeature(String featureName, PrefixResourcesManager nrManager, IResourceManager.Category category) {
        this(featureName, nrManager, category.toString());
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm) || !(sm.getReturnType() instanceof RefType))
            return FeatureResult.UNDEFINED;
        SootClass returnClass = ((RefType) sm.getReturnType()).getSootClass();
        return getNrManager().getNativeResourceCategory(returnClass).equalsIgnoreCase(category) ? FeatureResult.TRUE : FeatureResult.FALSE;
    }
}
