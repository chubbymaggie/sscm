package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.PrimType;
import soot.SootMethod;
import soot.VoidType;

public class PrimitiveReturnTypeFeature extends AbstractFeature {

    public PrimitiveReturnTypeFeature(String featureName, IResourceManager nrManager) {
        super(featureName, nrManager);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm) || sm.getReturnType().equals(VoidType.v()))
            return FeatureResult.UNDEFINED;

        return sm.getReturnType() instanceof PrimType ? FeatureResult.TRUE : FeatureResult.FALSE;
    }
}
