package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.SootMethod;

public class ClassNamePrefixFeature extends NamePrefixFeature {


    public ClassNamePrefixFeature(String featureName, IResourceManager nrManager, String prefix) {
        super(featureName, nrManager, prefix);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm))
            return FeatureResult.UNDEFINED;
        else
            return sm.getDeclaringClass().getName().startsWith(prefix) ? FeatureResult.TRUE : FeatureResult.FALSE;
    }

}
