package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.*;

/**
 * Models a feature that checks if the parameters of the given method are of a certain given type or, optionally, of
 * a child type.
 */
public class TypeParametersFeature extends ParametersFeature {

    private Type sootType;
    private boolean includeChildren;

    /**
     * Creates a new TypeParametersFeature thath checks if any of the parameter is of the given type.
     * @param featureName The name of the feature.
     * @param nrManager The native resource manager.
     * @param sootType The type to check for.
     * @param includeChildren Whether to include the children of the given type.
     */
    public TypeParametersFeature(String featureName, IResourceManager nrManager, Type sootType, boolean includeChildren) {
        super(featureName, nrManager);
        this.sootType = sootType;
        this.includeChildren = includeChildren;
    }

    /**
     * Creates a new TypeParametersFeature that checks if any of the parameter is of the given type, without including
     * the children of the given type.
     * @param featureName The name of the feature.
     * @param nrManager The native resource manager.
     * @param sootType The type to check for.
     */
    public TypeParametersFeature(String featureName, IResourceManager nrManager, Type sootType){
        this(featureName, nrManager, sootType, false);
    }


    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm) || sm.getParameterCount() == 0)
            return FeatureResult.UNDEFINED;
        else{
            for(Type t : sm.getParameterTypes()){
                if (t.equals(sootType) || isChildOf(t, sootType))
                    return FeatureResult.TRUE;
            }
            return FeatureResult.FALSE;
        }
    }

    private boolean isChildOf(Type child, Type parent){
        return includeChildren && Scene.v().getOrMakeFastHierarchy().canStoreType(child, parent);
    }
}
