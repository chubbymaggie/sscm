package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.*;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Checks if there is any dataflow from
 */
public class DataflowFromParamToThis extends DataflowFeature {

    public DataflowFromParamToThis(String featureName, IResourceManager nrManager) {
        super(featureName, nrManager);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm) || sm.isStatic())
            return FeatureResult.UNDEFINED;

        Body body = sm.retrieveActiveBody();
        HashSet<Value> paramLocals = new HashSet<>();
        paramLocals.addAll(body.getParameterLocals());
        paramLocals.removeIf(x-> !(x.getType() instanceof RefLikeType)); // remove non RefLike parameters
        HashMap<Unit, HashSet<Value>> result =  getTaintAnalyzer().taintAnalysisLocals(body, paramLocals);

        Local thisLocal = body.getThisLocal();
        for(Unit u : body.getUnits()){
            if (result.get(u).contains(thisLocal))
                return FeatureResult.TRUE;
        }

        return FeatureResult.FALSE;
    }
}
