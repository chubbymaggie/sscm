package org.unimib.sas.data.feature;


import org.unimib.sas.data.IResourceManager;
import soot.*;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.Stmt;

import java.util.HashMap;
import java.util.HashSet;

public class DataflowFromParamToNRM extends DataflowFeature {

    public DataflowFromParamToNRM(String featureName, IResourceManager nrManager) {
        super(featureName, nrManager);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm))
            return FeatureResult.UNDEFINED;

        Body body = sm.retrieveActiveBody();
        HashSet<Value> paramLocals = new HashSet<>();
        paramLocals.addAll(body.getParameterLocals());
        paramLocals.removeIf(x-> !(x.getType() instanceof RefLikeType)); // remove non RefLike parameters
        HashMap<Unit, HashSet<Value>> taintOut = getTaintAnalyzer().taintAnalysisLocals(body, paramLocals);
        HashMap<Unit, HashSet<Value>> taintIn = getTaintAnalyzer().getLastTaintIn();

        for (Unit u : body.getUnits()){
            try {
                Stmt stmt = (Stmt) u;
                if (stmt.containsInvokeExpr()) {
                    InvokeExpr invokeExpr = stmt.getInvokeExpr();
                    SootMethod invokedMethod = invokeExpr.getMethod();
                    // check if the invoked method is a NRM
                    if (getNrManager().isNativeResourceMethod(invokedMethod)) {
                        HashSet<Value> unitTaintIn = taintIn.get(u);
                        // Any of the NRM's args is tainted?
                        for (Value arg : invokeExpr.getArgs()) {
                            if (arg instanceof RefLikeType && unitTaintIn.contains(arg))
                                return FeatureResult.TRUE;
                        }

                        // The base object of the NRM is tainted?
                        if (invokeExpr instanceof InstanceInvokeExpr) {
                            if (taintOut.get(u).contains(((InstanceInvokeExpr) invokeExpr).getBase()))
                                return FeatureResult.TRUE;
                        }
                    }
                }
            } catch (ResolutionFailedException e){
                continue;
            }
        }
        return FeatureResult.FALSE;
    }
}
