package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.*;
import soot.jimple.InvokeExpr;
import soot.jimple.ReturnStmt;

import java.util.HashMap;
import java.util.HashSet;

public class DataflowFromNRMToReturn extends DataflowFeature {

    public DataflowFromNRMToReturn(String featureName, IResourceManager nrManager) {
        super(featureName, nrManager);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm) || sm.getReturnType().equals(VoidType.v()))
            return FeatureResult.UNDEFINED;

        Body body = sm.retrieveActiveBody();
        HashSet<InvokeExpr> nrmStatements = extractNativeResourceMethodStatements(body);
        HashMap<Unit, HashSet<Value>> taintOut = getTaintAnalyzer().taintAnalysisMethodCalls(body, nrmStatements);

        for (Unit u : body.getUnits()){
            if (u instanceof ReturnStmt){
                if(taintOut.get(u).contains(((ReturnStmt) u).getOp()))
                    return FeatureResult.TRUE;
            }
        }
        return FeatureResult.FALSE;
    }
}
