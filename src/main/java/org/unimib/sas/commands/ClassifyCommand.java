package org.unimib.sas.commands;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.unimib.sas.data.IFeatureSet;
import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.data.generators.WekaInstancesGenerator;
import org.unimib.sas.data.classification.WekaClassification;
import org.unimib.sas.data.nrm.CategoryResourceManager;
import org.unimib.sas.data.nrm.PrefixResourcesManager;
import org.unimib.sas.utils.Utils;
import org.unimib.sas.utils.soot.SootWrapper;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.Transformer;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.File;
import java.util.Collection;
import java.util.Map;

public class ClassifyCommand extends SootCommand {

    private String MODEL_FILE;
    private String INPUT_DIR;
    private String OUT_FILE;

    private final Option MODELFILE;
    private final Option INPUTDIR;
    private final Option OUTFILE;

    public ClassifyCommand() {
        super("classify");

        MODELFILE = Option.builder("m")
                .argName("model-file")
                .hasArg()
                .desc("The trained WEKA model file to use for classification.")
                .required()
                .build();

        INPUTDIR = Option.builder("i")
            .argName("input-dir")
            .hasArg()
            .required()
            .desc("The dir containing the JARs to classify. Single JAR file is accepted too.")
            .build();

        OUTFILE = Option.builder("o")
                .argName("output-file")
                .hasArg()
                .required()
                .desc("The file where to write the results. CSV format will be used.")
                .build();

        OPTIONS.addOption(OUTFILE);
        OPTIONS.addOption(MODELFILE);
        OPTIONS.addOption(INPUTDIR);
    }

    @Override
    protected void doOtherSootSetup() {
        SootWrapper.v.withProcessDir(Utils.listFiles(INPUT_DIR, "jar"));
    }

    @Override
    protected void doLoadOtherOptions(CommandLine cmdLine) {
        if (cmdLine.hasOption(MODELFILE.getOpt())){
            MODEL_FILE = cmdLine.getOptionValue(MODELFILE.getOpt());
            File file = new File(MODEL_FILE);
            if (!file.exists()){
                throw new RuntimeException("Model file (-m) does not exists.");
            }
        }

        if (cmdLine.hasOption(INPUTDIR.getOpt())){ //this option might be a duplicate of TARGET_DIR
            INPUT_DIR = cmdLine.getOptionValue(INPUTDIR.getOpt());
            File file = new File(INPUT_DIR);
            if (!file.exists())
                throw new RuntimeException("Input directory/file (-i) does not exists");
        }

        if (cmdLine.hasOption(OUTFILE.getOpt())){
            OUT_FILE = cmdLine.getOptionValue(OUTFILE.getOpt());
            File file = new File(OUT_FILE);
            if (file.isDirectory())
                throw new RuntimeException("Output file (-o) is not a file.");
            if (file.exists() && !file.canWrite())
                throw new RuntimeException("Output file (-o) is not writable.");
        }
    }

    @Override
    public Transformer getTransformer() {
        return new SceneTransformer() {
            @Override
            protected void internalTransform(String phaseName, Map<String, String> options) {
                IResourceManager nrm;
                IFeatureSet featureSet;
                switch (NRM){
                    case Normal:
                        nrm = new PrefixResourcesManager(RESOURCE_FILE);
                        break;
                    default:
                    case Category:
                        nrm = new CategoryResourceManager(RESOURCE_FILE);
                        break;

                }
                logger.info("Configuring resources using the current Scene...");
                nrm.initNativeResources();
                featureSet = FEATURE_SET.toIFeatureSet(nrm);

                WekaInstancesGenerator wig = new WekaInstancesGenerator(featureSet);
                Collection<SootClass> sootClasses = Scene.v().getApplicationClasses();

                logger.info("A total of {} classes will be scanned for input and target methods.", sootClasses.size());
                wig.generate(sootClasses);

                try {
                    Instances instances = wig.getDataset();
                    WekaClassification cls = WekaClassification.load(MODEL_FILE);
                    logger.info("Classifying {} methods using {} classifier.", instances.size(), cls.getClassifierName());

                    Instances classifiedInstances = cls.predict(wig.getDataset());
                    logger.info("Completed, writing results on file {}", OUT_FILE);

                    ConverterUtils.DataSink.write(OUT_FILE, classifiedInstances);
                    logger.info("Completed.");
                }catch (Exception e){
                    logger.error("An unexpected WEKA error occurred while writing the output file.");
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        };
    }
}
