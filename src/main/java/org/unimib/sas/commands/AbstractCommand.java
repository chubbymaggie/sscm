package org.unimib.sas.commands;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Abstract class for a cli command.
 */
public abstract class AbstractCommand implements ICommand {

    protected final Logger logger = LoggerFactory.getLogger("SSCM");

    private final static String FOOTER;
    private final static String HEADER;
    private final static int WIDTH;
    private final static String USAGE;
    private final static HelpFormatter formatter = new HelpFormatter();

    static {
        WIDTH = 80;
        HEADER = "Options: ";
        FOOTER = "Credits:\nThe tool is the Master's thesis project of "
                + "Darius Sas (darius.sas@outlook.com).\n" +
                "Supervisors: Prof. Francesca F. Arcelli, Marco Bessi." +
                "\nUniversita' degli Studi di Milano-Bicocca, Milano, Italy -- 2018";
        USAGE = "\tsscm <command-name> <command-options>\nCommands available:\n" +
                "\ttrain: Train a model and save it in a file.\n" +
                "\tgenerate: Generate a training file from a label file.\n" +
                "\tclassify: Classify JARs using the trained model.\n" +
                "\tlist: List all the methods within a given JAR or JAR folder.";
    }

    protected String commandName = "";
    protected org.apache.commons.cli.Options OPTIONS;
    protected final Option HELP;

    public AbstractCommand(String commandName){
        this.commandName = commandName;
        this.OPTIONS = new org.apache.commons.cli.Options();

        HELP = Option.builder("h")
                .longOpt("help")
                .desc("Prints this help.")
                .hasArg(false)
                .build();

        OPTIONS.addOption(HELP);
    }

    @Override
    public String getCommandName() {
        return commandName;
    }

    @Override
    public String getHelp() {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        formatter.printHelp(pw, WIDTH,
                "\n    sscm " + getCommandName() + " <command-options>",
                HEADER, OPTIONS, 4, 4,
                FOOTER);
        return sw.toString();
    }

    public org.apache.commons.cli.Options getOptions() {
        return OPTIONS;
    }

    /**
     * Loads the commands from the CommandLine object into the current object in order
     * to ease the execution of the command.
     * @param cmdLine the command line containing the commands.
     */
    protected abstract void loadOptions(CommandLine cmdLine);

    /**
     * Parses the args array and returns the corresponding ICommand object.
     * @param args the args array.
     * @return The ICommand object that implements the given command.
     */
    public static AbstractCommand parseArgs(String[] args){
        AbstractCommand command = null;
        if (args[0].equalsIgnoreCase("train")){
            command = new TrainCommand();
        }else if (args[0].equalsIgnoreCase("generate")) {
            command = new GenerateCommand();
        }else if (args[0].equalsIgnoreCase("classify")) {
            command = new ClassifyCommand();
        }else if (args[0].equalsIgnoreCase("list")){
            command = new ListCommand();
        } else {
            //System.out.println("Extract sources and sinks from arbitrary JAR files using Machine Learning.");
            if (!args[0].toLowerCase().contains("help"))
                System.out.printf("Command %s not recognized.\n", args[0]);
            System.out.printf("Usage: \n%s\n\n", USAGE);
            System.out.println("Use 'sscm <command> -help' for command specific help.");
            System.out.println(FOOTER);
            System.exit(0);
        }

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(command.getOptions(), Arrays.copyOfRange(args, 1, args.length));
            if (cmd.hasOption(command.HELP.getOpt()))
                throw new ParseException(command.getHelp());
            command.loadOptions(cmd);
        }catch (MissingOptionException e){
            List<String> options = Arrays.asList(args);
            options = options.stream().map(x-> x.replaceAll("-", "")).collect(Collectors.toList());
            if(options.contains(command.HELP.getOpt()) || options.contains(command.HELP.getLongOpt())){
                System.out.println(command.getHelp());
            }else {
                List<String> missingOpts = (ArrayList<String>) e.getMissingOptions();
                if (!missingOpts.isEmpty())
                    System.out.println("The following options are missing: ");
                for (String mOpt : missingOpts) {
                    System.out.printf("-%s: %s\n", mOpt, command.getOptions().getOption(mOpt).getDescription());
                }
            }
            System.exit(0);
        }catch (ParseException e){
            System.out.println(e.getMessage());
            System.exit(0);
        }
        return command;
    }

}
