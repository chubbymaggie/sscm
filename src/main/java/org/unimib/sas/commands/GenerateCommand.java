package org.unimib.sas.commands;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.unimib.sas.data.IFeatureSet;
import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.data.generators.WekaInstancesGenerator;
import org.unimib.sas.data.nrm.CategoryResourceManager;
import org.unimib.sas.data.nrm.PrefixResourcesManager;
import org.unimib.sas.utils.soot.SootWrapper;
import soot.*;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Generates a dataset that can be used to train a model.
 */
public class GenerateCommand extends SootCommand {

    private String TRAIN_LABELS;
    private String OUT_FILE;

    private final Option TRAINLABELS;
    private final Option OUTFILE;

    private Predicate<SootClass> classPredicate;
    private Predicate<SootMethod> methodPredicate;

    /**
     * Initializes a generate command object.
     */
    public GenerateCommand(){
        super("generate");
        TRAINLABELS = Option.builder("l")
                .argName("train-labels-file")
                .desc("The file containing the labeled training dataset for which to generate features.")
                .hasArg()
                .required()
                .build();

        OUTFILE = Option.builder("o")
                .argName("out-file")
                .hasArg()
                .required()
                .desc("The file where to write the generated feature.")
                .build();

        OPTIONS.addOption(OUTFILE);
        OPTIONS.addOption(TRAINLABELS);

        this.classPredicate = sc ->
                //!sc.isJavaLibraryClass()
                !sc.isPhantom()
                        && !sc.getName().startsWith("jdk")
                        && !sc.getName().startsWith("sun")
                        && !sc.getName().startsWith("com.sun");
        this.methodPredicate = sm ->
                //!sm.isConstructor()
                !sm.isStaticInitializer()
                        && !sm.isNative();
    }

    @Override
    protected void doLoadOtherOptions(CommandLine cmdLine) {
        if (cmdLine.hasOption(TRAINLABELS.getOpt())){
            TRAIN_LABELS = cmdLine.getOptionValue(TRAINLABELS.getOpt());
            File trainLabel = new File(TRAIN_LABELS);
            if (!trainLabel.exists() || !trainLabel.isFile()){
                throw new RuntimeException("Train labels file, -l option, is not a valid file: " + TRAIN_LABELS);
            }
        }

        if (cmdLine.hasOption(OUTFILE.getOpt())){
            OUT_FILE = cmdLine.getOptionValue(OUTFILE.getOpt());
            File file = new File(OUT_FILE);
            if (file.isDirectory())
                throw new RuntimeException("Output file (-o) is not a file.");

            if (file.exists() && !file.canWrite())
                throw new RuntimeException("Output file (-o) is not writable.");
        }
    }

    @Override
    protected void doOtherSootSetup() {
        SootWrapper.v.withProcessDir(CLASSPATH_); //it should allow to generate everything using the classpath...
    }

    @Override
    public Transformer getTransformer() {
        return new SceneTransformer() {
            @Override
            protected void internalTransform(String s, Map<String, String> map) {
                IResourceManager nrm;
                IFeatureSet featureSet;
                switch (NRM){
                    case Normal:
                        nrm = new PrefixResourcesManager(RESOURCE_FILE);
                        break;
                    default:
                    case Category:
                        nrm = new CategoryResourceManager(RESOURCE_FILE);
                        break;

                }
                logger.info("Configuring resources using the current Scene...");
                nrm.initNativeResources();
                featureSet = FEATURE_SET.toIFeatureSet(nrm);
                WekaInstancesGenerator wig = new WekaInstancesGenerator(featureSet, classPredicate, methodPredicate);
                try {
                    logger.info("Reading training labels from {}", TRAIN_LABELS);
                    Instances instances = ConverterUtils.DataSource.read(TRAIN_LABELS);

                    logger.info("Generating features data for {} methods", instances.size());
                    logger.info("using feature set {}...", FEATURE_SET);

                    wig.generate(instances);

                    logger.info("Completed, writing on file {}", OUT_FILE);

                    instances = wig.getDataset();
                    ConverterUtils.DataSink.write(OUT_FILE, instances);

                    logger.info("Completed.");
                }catch (Exception e){
                    System.out.println("Unexpected error occurred while reading or writing methods file.");
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        };
    }
}
