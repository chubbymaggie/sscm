package org.unimib.sas.commands;

import soot.Transformer;

/**
 * Models a cli command that will be executed.
 */
public interface ICommand {
    /**
     * Returns the name of the command.
     * @return a string with the name of the command.
     */
    String getCommandName();

    /**
     * Returns the help string of the command.
     * @return A formatted string of the help of the command.
     */
    String getHelp();

    /**
     * Returns the transformer object that can be fed to soot to be executed as a within its phases.
     * @return
     */
    Transformer getTransformer();

    /**
     * The implementers of this methods will execute the command that it is representing.
     * It must take care of the correct configuration of soot.
     */
    void run();
}
