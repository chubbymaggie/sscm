# Source Sink Classification Model -- SSCM

## Table of contents
- [Introduction](#introduction)
- [Usage](#usage)
- [Installation and Requirements](#installation)
- [Known Issues](#issues)
- [Classification Process](#classification)
- [References](#references)
- [Credits](#credits)

## Introduction
SSCM is a machine learning-based tool that can be used to identify data sources and data sinks from arbitrary Java libraries.
On the test set, the tool achieved 81% F1-score and 86% accuracy on a validation set of over 600 methods [4].

However, the tool can only be as good as the dataset the internal Machine Learning model was trained on. Thus, for optimal results, it is highly recommended to consider calibrating the tool by fine-tuning the configuration file and create a *specialized* training set.

## Usage
The intended use of the tool is the following:
    
1. `generate` a dataset and `train` a default model using the default given label file.
2. `classify` the JARs you need, and
3. improve the model by:
    - adding new methods to the dataset or building your own dataset, or
    - fine-tuning the Resources' description adding categories and other Resource Classes relative to the type of JARs that you are analyzing, or
    - creating your own machine learning model by directly selecting the best model you need by directly passing to [WEKA][weka] [3] its dedicated commands.

Hence, SSCM provides three main working modes, which translate into three different command line commands, each with his own parameters:

 1. `generate` - Used to calculate features value for a labeled training set.
 In particular, the command expects a file containing the pairs `<methodName, tag>` and will generate a file containing the tuples `<methodName, tag, features*>`.
    
    As we can see from the picture below, the generate command expects three required input parameters:

    - `-cp` is the classpath of SSCM. It can be a folder or a `.jar` file and *must* contain all the classes referenced in the **Resources' description** file. If this parameter is a folder, `sscm` will recursively add all the `.jar` files from any subfolder.
    - `-l` is the file containing the method names and their relative labels (namely the labeled dataset without the feature values). More info about the format of this file below.
    - `-r` is the **Resources' description** file that describes the resource classes, which are the known classes that perform **direct** I/O operations. A default file is provided by default. This file can change its content (but not format) from the one used during training. More info about this file below.

    And one *optional* parameter:
    
    - `-fs` represents the feature set to use to generate the feature data. This option should not be changed unless you know what are you doing. The definition of feature set can be found in the Definitions section.

    The output parameter is:
    
    - `-o` is the file where to write the training data. This is will be a CSV file, details about its internal format can be found into the Files and Formats section below.


 2. `train` - Train a [WEKA][weka] [3] model using the output file from the `generate`  command and save it to file to reuse.
    
    The train command is pretty straightforward:

    - `-t` is the training file previously generated with the `generate` command.
    - `-o` is the file where to write the *trained* WEKA model.

    The option specifying the model configuration can be omitted, and a default pre-tested model will be used:

    - `-m` can be either the full class name of a WEKA *classifier* or more detailed WEKA options to use for building the classifier. This parameter is optional, the default value instantiates a SVM with a polynomial kernel. In case you specify a kernel remember that the kernel's options must be enclosed in quotes (ex.: `[...] -K "weka.package.Kernel -C 22 -L"`). See [WEKA][weka] documentation page or consult the **[manual](./guide/WekaManual-3.8.1.pdf)** for more.


 3. `classify` - Given a JAR file or a folder of JARs, classifies any methods found in such files as `input` (for source methods), `target` (for sink methods), `mixed` (for methods with mixed behavior) or `none`.
    
    The classify command has the following *mandatory* parameters:
    
    - `-cp` is the classpath of SSCM. It can be a folder or a `.jar` file and *must* contain all the classes referenced in the **Resources' description** file. If this parameter is a folder, `sscm` will recursively add all the `.jar` files from any subfolder.
    - `-m` the file containing the serialized model previously trained with the command `train`.
    - `-i` this option indicates the input for the analysis, all the methods found in this path will be classified. Can be either a folder containing `.jar` files or a `.jar` file. If it is a folder, it will be recursively enumerated and all `.jar` files will be added to analysis.
    - `-o` the file where to save the results.
    - `-r` is the **Resources' description** file that describes the resource classes, which are the known classes that perform **direct** I/O operations. A default file is provided by default. This file can change its content (but not format) from the one used during training. More info about this file below.
    
    And one *optional* parameter:
    
    - `-fs` represents the feature set to use to generate the feature data. This option should not be changed unless you know what are you doing. Note that this must match the one used with the `generate` command, if used. The definition of feature set can be found in the Classification Model section.

The following simplified activity diagram describes the modes of working of SSCM and the arguments of each mode. Note that once you have generated the data and trained a model with it, you can freely classify JARs without the need to repeat the generation and the training.

![activity-diagram](./guide/img/activity-diagram.png "The activity diagram of SSCM.")

More information about the actual usage is available using the help option:
```
> java -jar sscm.jar help                // for the list of commands
> java -jar sscm.jar <command> -help     //for the help of the specific command.
```

### Files and Formats
The file formats used by SSCM are usually CSV (Comma Separated Value) and YAML (YAML Ain't Markup Language).

The CSV format in particular, SSCM uses the WEKA conventions, thus, the comma ("`,`") character is the column separator and the double quotes ("`"`") character is used to escape text.

The format of the methods' name in the CSV files is the *method signature* adopted by [Soot][soot] [2], which is:
```
<full.package.ClassName: full.package.ReturnType methodName(full.package.Param1, ...)>
```

The headers of the CSV input files can be described as follow:
```
methodSignature,tag,featureHeaders*
```
where `featureHeaders*` represents a variable number of features (and thus headers), from 0 to the number of features defined by the feature set used.

Usually the "Training label" file used during generation will have 0 features, while the resulting file "Training data" will have one or more feature headers.

As a side note, remember that methods' signatures must be escaped with `"`.

In regard to the YAML resource file ("Resources' description"), it contains a description of how known I/O classes of the Scene under analysis are being categorized. In this file classes are identified by their full name (e.g. `full.package.ClassName`).

## Installation and Requirements
There are two ways to use SSCM, the first is by compiling it by yourself, using Maven, and the second one is by using the JAR available in the **[deploy](./deploy/)** directory.

The Maven build can be simply done with the following commands:
```
> git clone https://darius-sas@bitbucket.org/darius-sas/sscm.git 
> cd sscm
> mvn clean compile assembly:single
```
This will produce a `sscm-jar-with-dependencies.jar` in the `target` directory of your local copy of the repository. The file will contain all the necessary dependencies to run.
The only requirements to compile SSCM are Maven 3.5.x and a Java 8 compiler.

## Known issues
Below are listed the current known issues:

1. The build system presents several issues and has a lot of room for improvement.
2. A serious testing strategy is necessary. The old tests used to test the code were not included in this repository since they required too much refactoring and they were not worth the effort.
3. [...]

Should you encounter any issue, please report it.

## Classification process
### Definitions
Below there are reported some useful definitions that SSCM uses to mine for sources and sinks.

#### Scene
The Scene is the term used by Soot to refer to all the classes available for analysis. If a class is not present in the Scene but is referenced, it is called a **phantom class**. Phantom classes, especially if they are resources, can drastically change the result of the analysis.

#### Resource class
A resource represents a class or an interface that implements or declares generic I/O operations on an underlying resource (network, hard disk, database, etc.).
At the moment resource classes, or more specifically interfaces and abstract classes, have to be manually provided as input. SSCM automatically derives children and implementing classes that extend or implement a given resource class, reducing the amount of manual work required by the user. Future works plan to further automate this aspect.

#### Resource method
A resource method is a method of a resource class that performs I/O operations. Resource methods represent the lowest level of I/O operations available in the Scene. SSCM uses the connections and interactions of other methods and classes with the resources and their methods to mine sources and sinks.

#### Feature set
A set of feature function that encodes a method into its representation in the feature space.
Different representation of the feature space will result in different results. At the moment there are 4 different feature sets defined in SSCM, but we highly recommend to use the `Category` one.

### The model
The following image briefly summarizes the features that SSCM uses to classify every method that analyzes.

![classification](./guide/img/classification-data-points.png)

We can observe the following categories:

- Prefix-based features: they use a restricted set of prefixes that contain prefixes that are well-known for being commonly used for retrieving or writing data from high-level or low-level resources. For this reason this category of features usually correlates the most with the correct classification.

- Dependency-based features: they exploit the inheritance-related and parameter type-based dependencies with the given resource classes and their children/implementers.

- Dataflow-based features: they account for the internal dataflow of the method, especially if it invokes a resource method. Specifically, the following dataflow cases, represented by '`-->`', are considered:
    
    - method's parameters `-->` resource method
    - this `-->` method's parameters
    - resource method `-->` to method's return
    - resource method `-->` to method's parameter
    - resource method `-->` to this

- Keywords and Modifiers: represent the last category of features used by SSCM. The Keywords category recognizes if certain keywords, specified by the user, are present inside the full name of the declaring class. For example, classes that contain '`http`' in their full name are more likely to contain I/O operations than one that do not contain any keyword. Modifiers, on their own, provide more insights for the final classification.

Such features will be used to generate a number of different features that will be used by the actual classifier to predict the class of each method.
### Resource representation
It is clear at this point that the Resources are a key part of the classification process of SSCM. Their description drastically influences the results of the analysis.

The current representation has the advantage of being easily extended (within certain boundaries), without requiring to retrain the model. This is very important, since it allows for future works to develop an automatic approach for SSCM mining resource classes and add them to the initial resource set, described by the user.

We avoid formally describing the representation and prefer a more practical explanation.
Take a look at the current default representation: the `.yml` file in the **[deploy](./deploy/categorized-resources.yml)** directory.
As we can note the file has a map named `resource_categories` where each key is the name of the category and the values are the classes belonging to such category. These classes are the initial resource classes considered by SSCM, which will eventually be used to derive the complete set by navigating (downwards) the inheritance tree of each class.
Thus, a possible way to improve the recognition power of the model is to add the well-known classes from the JARs under analysis that model the I/O operations.

The other entries in the resource file are the 6 types of prefixes (described by their comments) and the `class_name_patterns` entry, which helps correlating certain keywords with certain resource class categories. As for the `resource_categories` entries, adding keywords will not require retraining the model, but adding keys will do.


## Related works
SSCM is partially based on the work made by Arzt et al. with [SuSi][susi] [1]. SuSi is a source and sink classification tool specifically designed for the Android Framework.

## References
[1] Steven Arzt, Siegfried Rasthofer, and Eric Bodden. Susi: A tool for the fully automated classification and categorization of Android sources and sinks. Technical Report TUD-CS-2013-0114, EC SPRIDE, 2013.

[2] Raja Vallée-Rai, Phong Co, Etienne Gagnon, Laurie Hendren, Patrick Lam, and Vijay Sundaresan. 1999. Soot - a Java bytecode optimization framework. In Proceedings of the 1999 conference of the Centre for Advanced Studies on Collaborative research (CASCON '99), Stephen A. MacKay and J. Howard Johnson (Eds.). IBM Press 13.

[3] Mark Hall, Eibe Frank, Geoffrey Holmes, Bernhard Pfahringer, Peter Reutemann, and Ian H. Witten. 2009. The WEKA data mining software: an update. SIGKDD Explor. Newsl. 11, 1 (November 2009), 10-18. 

[4] Darius Sas, Marco Bessi, and Francesca Arcelli Fontana. 2018. Automatic Detection of Sources and Sinks in Arbitrary Java Libraries. Accepted at SCAM 2018.

## Credits
Master Thesis Project of:

*Darius Sas* - Università degli Studi di Milano-Bicocca

Supervisors:

*Prof. Francesca Fontana Arcelli* - Università degli Studi di Milano-Bicocca

*PhD Marco Bessi* - CAST Software Italia

2018

[soot]: https://github.com/Sable/soot
[susi]: https://blogs.uni-paderborn.de/sse/tools/susi/
[weka]: https://www.cs.waikato.ac.nz/ml/weka/